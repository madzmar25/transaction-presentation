<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home', [App\Http\Controllers\HomeController::class, 'showHomepage'])->name('home');
Route::get('/reports/{pid?}/{selected?}', [App\Http\Controllers\PagesController::class, 'showReports'])->name('admin.reports');
Route::post('/reports/search', [App\Http\Controllers\TransactionController::class, 'dailyLogSearch'])->name('reports.search');
Route::get('/settings', [App\Http\Controllers\PagesController::class, 'showSettings'])->name('admin.settings');
Route::get('/settings/graphs', [App\Http\Controllers\PagesController::class, 'showSettingGraphs'])->name('admin.settings.graphs');
Route::get('/setting/users', [App\Http\Controllers\Auth\UserController::class, 'index'])->name('admin.users');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
Route::post('/setting/transaction/upload', [App\Http\Controllers\TransactionController::class, 'upload'])->name('setting.transaction.upload');
Route::post('/setting/transaction/search', [App\Http\Controllers\TransactionController::class, 'search'])->name('setting.transaction.search');
Route::get('/template/download/{filename}', [App\Http\Controllers\PagesController::class, 'download'])->name('template.download');

Route::post('/user', [App\Http\Controllers\Auth\UserController::class, 'registerUser'])->name('user.register');
Route::get('/user', [App\Http\Controllers\Auth\UserController::class, 'list'])->name('user.list');
Route::get('/user/{user_id}', [App\Http\Controllers\Auth\UserController::class, 'edit'])->name('user.edit');
Route::delete('/user/{user_id}', [App\Http\Controllers\Auth\UserController::class, 'deleteUser'])->name('user.delete');
Route::put('/user/{user_id}', [App\Http\Controllers\Auth\UserController::class, 'updateUser'])->name('user.update');
Route::get('/', [App\Http\Controllers\HomeController::class, 'showHomepage'])->name('home');
Auth::routes();
