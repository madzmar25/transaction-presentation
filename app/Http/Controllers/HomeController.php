<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Controllers\Pages;
use App\Repositories\TransactionSettingRepository;
use App\Repositories\DailyTransactionRepository;
use App\Repositories\UserRepository;

class HomeController extends Controller
{
    use Pages;
    public $transactionSettingRepository;
    public $dailyTransactionRepository;
    public $userRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        TransactionSettingRepository $transactionSettingRepository,
        DailyTransactionRepository $dailyTransactionRepository,
        UserRepository $userRepository
    ){
        $this->transactionSettingRepository = $transactionSettingRepository;
        $this->dailyTransactionRepository   = $dailyTransactionRepository;
        $this->userRepository               = $userRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
