<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Controllers\Transaction;
use App\Repositories\TransactionSettingRepository;
use App\Repositories\LoginSettingRepository;
use App\Repositories\DailyTransactionRepository;

class TransactionController extends Controller
{
    use Transaction;
    public $transactionSettingRepository;
    public $dailyTransactionRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        TransactionSettingRepository $transactionSettingRepository,
        DailyTransactionRepository $dailyTransactionRepository,
        LoginSettingRepository $loginSettingRepository
    ){
        $this->transactionSettingRepository = $transactionSettingRepository;
        $this->loginSettingRepository = $loginSettingRepository;
        $this->dailyTransactionRepository = $dailyTransactionRepository;
    }
}
