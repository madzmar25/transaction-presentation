<?php
/**
 * Class TransactionSettingRepository
 */

namespace App\Repositories;

use App\Helpers\Utility;
use App\Models\TransactionSetting;
use Hamcrest\Util;
// use App\Transformers\FieldTransformer;
use Illuminate\Support\Str;
// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;

/**
 * Class UserRepository
 */
class TransactionSettingRepository
{
    const LOGIN       = "login";
    const TRANSACTION = "transaction";
    // use FieldTransformer;
    protected $model;
    // protected $departmentRepository;
    // protected $sectionRepository;

    public function __construct(TransactionSetting $model)
    {
        $this->setModel($model);
        // $this->departmentRepository = $departmentRepository;
        // $this->sectionRepository    = $sectionRepository;
    }

    /**
     * Set the model to be use for this repository
     *
     * @param $model App\Models\Section
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the assigned model for this repository
     *
     * @return App\Models\Section
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getData($date, $type, $is_peak = 0)
    {
        $transactionSettings = $this->getModel()::where('date', $date)
            ->where('is_peak', $is_peak)
            ->where('type', $type)
            ->get()
            ->toArray();

        return $transactionSettings;
    }

    public function getDateTimeData($date, $type, $is_peak = 0)
    {
        $transactionSettings = $this->getModel()::where('date', $date)
            ->where('is_peak', $is_peak)
            ->where('type', $type)
            ->get()
            ->toArray();

        return $transactionSettings;
    }

    public function getMonthSettings($date, $type)
    {
        $dateTime = strtotime($date);
        $transactionSettings = $this->getModel()::where('date', '>=', $date)
            ->where('date', '<', date('Y-m-d', strtotime("+1 month", $dateTime)))
            ->where('type', $type)
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();

        return $transactionSettings;
    }
}
