<?php
/**
 * Class DailyTransactionRepository
 */

namespace App\Repositories;

use App\Models\DailyTransaction;

/**
 * Class UserRepository
 */
class DailyTransactionRepository
{
    protected $model;

    public function __construct(DailyTransaction $model)
    {
        $this->setModel($model);
    }

    /**
     * Set the model to be use for this repository
     *
     * @param $model App\Models\Section
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the assigned model for this repository
     *
     * @return App\Models\Section
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getData($date, $type)
    {
        $data = $this->getModel()::where('date', $date)
            ->where('type', $type)
            ->get()
            ->toArray();
        return $data;
    }

    public function getDataByPID($pid)
    {
        $data = $this->getModel()::where('pid', $pid)
            ->first();
        return $data;
    }
}
