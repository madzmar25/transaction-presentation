<?php
/**
 * Class TransactionSettingRepository
 */

namespace App\Repositories;

use App\Models\LoginSetting;
// use App\Transformers\FieldTransformer;
use Illuminate\Support\Str;
// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;

/**
 * Class UserRepository
 */
class LoginSettingRepository
{
    // use FieldTransformer;
    protected $model;
    // protected $departmentRepository;
    // protected $sectionRepository;

    public function __construct(LoginSetting $model)
    {
        $this->setModel($model);
        // $this->departmentRepository = $departmentRepository;
        // $this->sectionRepository    = $sectionRepository;
    }

    /**
     * Set the model to be use for this repository
     *
     * @param $model App\Models\Section
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the assigned model for this repository
     *
     * @return App\Models\Section
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getData($date, $is_peak = 0)
    {
        $transactionSettings = $this->getModel()::where('date', $date)
            ->where('is_peak', $is_peak)
            ->get()
            ->toArray();

        return $transactionSettings;
    }

    public function getMonthSettings($date)
    {
        $dateTime = strtotime($date);
        $transactionSettings = $this->getModel()::where('date', '>=', $date)
            ->where('date', '<', date('Y-m-d', strtotime("+1 month", $dateTime)))
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();

        return $transactionSettings;
    }
}
