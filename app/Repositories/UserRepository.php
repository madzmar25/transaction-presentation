<?php
/**
 * Class UserRepository
 */

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepository
 */
class UserRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->setModel($model);
    }

    /**
     * Set the model to be use for this repository
     *
     * @param $model App\Models\Section
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the assigned model for this repository
     *
     * @return App\Models\Section
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getData($limit = 0)
    {
        if (!$limit) {
            $users = $this->getModel()::get();
            return $users;
        }

            // ->toArray();
        $users = $this->getModel()->orderByDesc('created_at')->limit($limit)->get();
        return $users;
    }

    public function getDataById($pid)
    {
        $user = $this->getModel()::where('pid', $pid)->first();

        return $user;
    }

    public function update($user, $data)
    {
        if (isset($data['firstname'])) {
            $user->firstname = $data['firstname'];
        }
        if (isset($data['lastname'])) {
            $user->lastname = $data['lastname'];
        }
        if (isset($data['email'])) {
            $user->email = $data['email'];
        }
        if (isset($data['status'])) {
            $user->is_active = $data['status'];
        }
        if (isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }

        return $user->save();
    }

    public function delete($user)
    {
        return $user->delete();
    }
}
