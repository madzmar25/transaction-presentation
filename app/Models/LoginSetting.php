<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginSetting extends Model
{
    use HasFactory;

    protected $table = 'login_settings';
    protected $guarded = array();
}
