<?php

namespace App\Providers;

use App\Helpers\Utility;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class UtilityProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('utility',function(){
            return new Utility();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
