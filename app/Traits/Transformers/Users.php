<?php
namespace App\Traits\Transformers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;
// use App\Transformers\DepartmentTransformer;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
trait Users
{
    public function transform($users, $is_single = false)
    {
        $formatted = [];
        if ($is_single) {
            $formatted = [
                'firstname' => (is_array($users) ? Arr::get($users, 'firstname', '') : $users->firstname),
                'lastname'  => (is_array($users) ? Arr::get($users, 'lastname', '') : $users->lastname),
                'email'     => (is_array($users) ? Arr::get($users, 'email', '') : $users->email),
                'status'    => (is_array($users) ? Arr::get($users, 'is_active', 'Inactive') : $users->is_active),
                'pid'       => (is_array($users) ? Arr::get($users, 'pid', '') : $users->pid)
            ];

            return $formatted;
        }

        if (is_array($users)) {

            foreach($users as $user) {
                $formatted[] = [
                    'firstname' => (is_array($users) ? Arr::get($users, 'firstname', '') : $user->firstname),
                    'lastname'  => (is_array($users) ? Arr::get($users, 'lastname', '') : $user->lastname),
                    'email'     => (is_array($users) ? Arr::get($users, 'email', '') : $user->email),
                    'status'    => (is_array($users) ? Arr::get($users, 'is_active', 'Inactive') : $user->is_active),
                    'pid'       => (is_array($users) ? Arr::get($users, 'pid', '') : $user->pid)
                ];
            }
        } else {

            foreach($users as $user) {
                $formatted[] = [
                    'firstname' => (is_array($user) ? Arr::get($user, 'firstname', '') : $user->firstname),
                    'lastname'  => (is_array($user) ? Arr::get($user, 'lastname', '') : $user->lastname),
                    'email'     => (is_array($user) ? Arr::get($user, 'email', '') : $user->email),
                    'status'    => (is_array($user) ? Arr::get($user, 'is_active', 'Inactive') : $user->is_active),
                    'pid'       => (is_array($user) ? Arr::get($user, 'pid', '') : $user->pid)
                ];
            }
        }

        return (count($formatted) == 1) ? array_shift($formatted) : $formatted;
    }

    public function transformToDataTable($users)
    {
        $formatted = [];

        if (is_array($users)) {
            foreach($users as $user) {
                $formatted[] = [
                    Arr::get($user, 'firstname', ''),
                    Arr::get($user, 'lastname', ''),
                    Arr::get($user, 'email', ''),
                    Arr::get($user, 'status', 'Inactive'),
                    Arr::get($user, 'pid', ''),
                ];
            }
        } else {
            $formatted[] = [
                Arr::get($users, 'firstname', ''),
                Arr::get($users, 'lastname', ''),
                Arr::get($users, 'email', ''),
                Arr::get($users, 'status', 'Inactive'),
                Arr::get($users, 'pid', ''),
            ];
        }

        return $formatted;
    }
}

