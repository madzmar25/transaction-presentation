<?php
namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use App\Helpers\UtilityFacade as Utility;
// use App\Transformers\DepartmentTransformer;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
trait Graphs
{
    public function getPeakData($date, $type)
    {
        $peakData = $this->transactionSettingRepository->getData($date, $type, 1);
        return $peakData;
    }

    public function getNonPeakData($date, $type)
    {
        $nonPeakData = $this->transactionSettingRepository->getData($date, $type, 0);
        return $nonPeakData;
    }

    public function groupByStatus($data)
    {
        $data_array = [];

        if (!$data) {
            return $data_array;
        }

        foreach($data as $record){
            $status = $record["status"];

            $data_array[Utility::toKeywordFormat($status)] = $record;
            if (isset($record["is_peak"])) {
                $data_array['is_peak'] = $record["is_peak"];
            }
        }

        return $data_array;
    }

    /**
     * Retrieve all settings for the month. This includes the peak and the default value
     *
     * @param $date string
     * @return Array
     */
    public function getMonthSettings($date, $type)
    {
        $monthSettings = $this->transactionSettingRepository->getMonthSettings($date, $type);
        $formatted = [];
        // Format by date first
        foreach($monthSettings as $data)
        {
            if (!isset($formatted[$data['date']])) {
                $formatted[$data['date']] = [];
            }

            $formatted[$data['date']][] = $data;
        }

        // Group the entry by status and assign to the date
        foreach($formatted as $key => $data) {
            $formatted[$key] = $this->groupByStatus($data);
        }
        return $formatted;
    }

    public function getDaysData($date, $type)
    {
        $formatted = [];
        $data_setting = [];
        $data_setting = $this->getPeakData($date, $type);

        if (!$data_setting) {
            $newdate = date("Y-m-01", strtotime($date));
            $data_setting = $this->getNonPeakData($newdate, $type);
        }

        if ($data_setting) {
            $formatted['settings'] = $this->groupByStatus($data_setting);
        }

        $formatted['data'] = $this->groupByStatus($this->dailyTransactionRepository->getData($date, $type));
        return $formatted;
    }

    public function getAverage($data, $settings, $type)
    {
        $statuses = config('transaction_type.' . $type);
        $percentage = [];
        $columns = config('transaction_type.time_column');
        foreach($statuses as $status)
        {
            $data_stats = isset($data[$status]) ? $data[$status] : [];
            $settings_stats = [];
            if ($settings) {
                $settings_stats = isset($settings[$status]) ? $settings[$status] : [];
            }

            foreach($columns as $column) {
                $value1 = isset($data_stats[$column]) ? $data_stats[$column] : 0;
                $value2 = 0;
                if ($settings_stats && isset($settings_stats[$column])) {
                    $value2 = $settings_stats[$column];
                }

                $percentage[$status][$column] = Utility::getAverage($value1, $value2);
            }

        }

        return $percentage;
    }

    public function getStatuses($data, $settings, $type)
    {
        $statuses = config('transaction_type.' . $type);
        $status_arr = [];
        $columns = config('transaction_type.time_column');
        foreach($statuses as $status)
        {
            $data_stats = isset($data[$status]) ? $data[$status] : [];
            $settings_stats = isset($settings[$status]) ? $settings[$status] : [];

            foreach($columns as $column) {
                $value1 = isset($data_stats[$column]) ? $data_stats[$column] : 0;
                $value2 = isset($settings_stats[$column]) ? $settings_stats[$column] : 0;
                $status_arr[$status][$column] = Utility::getStatusClass($value1, $value2);
            }

        }

        return $status_arr;
    }
}

