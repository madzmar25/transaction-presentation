<?php
namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use App\Imports\TransactionSettingsImport;
use App\Imports\LoginSettingsImport;
use App\Helpers\UtilityFacade as Utility;

// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;
// use App\Transformers\DepartmentTransformer;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
trait Transaction
{
    use Graphs;
    // use DepartmentTransformer;
    public function upload(Request $request)
    {
        $file = $request->file('upload_file');
        $type = $request->get('report_type');

        switch($type) {
            case "transaction":
                \Excel::import(new TransactionSettingsImport($this->transactionSettingRepository->getModel()), $file);
                break;
            case "login":
                // A custom settingRepository can be added here if you have custom settings for this settings type.
                \Excel::import(new LoginSettingsImport($this->transactionSettingRepository->getModel()), $file);
                break;
            default:
                return back()->withStatus('Invalid report type!');
        }

        return back()->withStatus('Excel file imported successfully!');
    }

    public function search(Request $request)
    {
        $date = $request->get('date', null);
        $type = $request->get('report_type', '');

        if (!$date || !$type || !Utility::isValidType($type)) {
            $view = view('partials.settings.graphs')->with('tabs', [])->render();

            return response()->json([
                'view' => $view
            ]);
        }

        $dateArray = explode("/", $date);
        $current_year = $dateArray[1];
        $current_month = $dateArray[0];
        // Retrieve the first day of month and it will add a month when retrieving the value
        $monthSettings = $this->getMonthSettings($current_year . "-" . $current_month . "-01", $type);
        $view = view('partials.settings.graphs')->with(['tabs'=> $monthSettings, 'type' => $type])->render();

        return response()->json([
            'view' => $view
        ]);
    }

    public function dailyLogSearch(Request $request)
    {
        $date = $request->get('date', null);
        $type = $request->get('report_type', "");

        if (!$date || !$type || !Utility::isValidType($type)) {

            $view = view('partials.reports.index')->with('data', [])->render();

            return response()->json([
                "success" => false,
                'view' => $view,
                "message" => "Invalid parameter. Please check the date or report type value!"
            ], 400);
        }

        $dateArray = explode("/", $date);
        $current_date = $dateArray[1];
        $current_month = $dateArray[0];
        $current_year = $dateArray[2];

        $date_now = $current_year . "-" . $current_month . "-" . $current_date;

        $data = $this->getDaysData($date_now, $type);
        $data['percentage'] = $this->getAverage($data['data'], $data['settings'], $type . "_status");
        $data['class_status'] = $this->getStatuses($data['data'], $data['settings'], $type . "_status");
        $columns = config('transaction_type.' . $type . '_status');
        $timeColumn = config('transaction_type.time_column');

        $view = view('partials.reports.index')->with([
            'data'        => $data,
            'type'        => $type,
            'columns'     => $columns,
            'time_column' => $timeColumn
        ])->render();

        return response()->json([
            'view' => $view
        ]);
    }
}

