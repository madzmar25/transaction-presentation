<?php
namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\TransactionSettingRepository;
use App\Helpers\UtilityFacade as Utility;
use DateTime;
use DateTimeZone;
// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;
// use App\Transformers\DepartmentTransformer;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
trait Pages
{
    use Graphs;
    // use DepartmentTransformer;
    public function showHomepage(Request $request)
    {
        $current_date  = date('d');
        $current_month = date('m');
        $current_year  = date('Y');
        $time          = date('H');
        $date_now      = $current_year . "-" . $current_month . "-" . $current_date;

        $dataTransaction = $this->getDaysData($date_now, TransactionSettingRepository::TRANSACTION);
        $dataLogin       = $this->getDaysData($date_now, TransactionSettingRepository::LOGIN);
        $users = $this->userRepository->getData(10);
        $columnTime = Utility::mapTimeToColumnName($time);
        $settingsLogin = [];
        $settingsTransaction = [];

        if (isset($dataLogin['settings'])) {
            $settingsLogin = $dataLogin['settings'];
        }

        if (isset($dataTransaction['settings'])) {
            $settingsTransaction = $dataTransaction['settings'];
        }
        // Get average
        $dataLogin['percentage'] = $this->getAverage($dataLogin['data'], $settingsLogin, "login_status");
        $dataTransaction['percentage'] = $this->getAverage($dataTransaction['data'], $settingsTransaction, "transaction_status");

        $dataLogin['class_status'] = $this->getStatuses($dataLogin['data'], $settingsLogin, "login_status");
        $dataTransaction['class_status'] = $this->getStatuses($dataTransaction['data'], $settingsTransaction, "transaction_status");

        if ($request->ajax()) {
            $user_table = view('page.dashboard.user_table')->with('users', $users)->render();
            return response()->json([
                'transaction' => $dataTransaction,
                'login' => $dataLogin,
                'users' => $user_table,
                'columnTime' => $columnTime,
            ]);
        }
// dd($dataLogin);
        return view('page.dashboard',
            [
                'pagename'  => 'Dashboard',
                'data'      => $dataTransaction,
                'dataLogin' => $dataLogin,
                'users'     => $users,
                'columnTime' => $columnTime,
                'page_class'  => "dashboard",
            ]
        );
    }
    // use DepartmentTransformer;
    public function showSettings()
    {
        return view('page.dashboard');
    }

    // use DepartmentTransformer;
    public function showUsers()
    {
        return view('page.users.index', ['pagename' => 'Users']);
    }

    public function showSettingGraphs()
    {
        // $graphData = $this->getData();
        $current_date = date('d');
        $current_month = date('m');
        $current_year = date('Y');
        $date_now = $current_year . "-" . $current_month . "-" . $current_date;
        $report_type = config('transaction_type.type');
        $monthSettings = $this->getMonthSettings($current_year . "-05-01", TransactionSettingRepository::TRANSACTION);
        $type = TransactionSettingRepository::TRANSACTION;
        return view('page.graphs.settings',
            [
                'pagename' => 'Graphs Settings',
                'report_type' => $report_type,
                'type' => $type,
                'tabs' => $monthSettings,
                'page_class'  => "settingsGraph",
            ]);
    }

    public function showReports($pid = "", $selected = "")
    {
        $tab_selected = $selected;
        $current_date = date('d');
        $current_month = date('m');
        $current_year = date('Y');
        $date_now = $current_year . "-" . $current_month . "-" . $current_date;
        $type = TransactionSettingRepository::TRANSACTION;
        $report_type = config('transaction_type.type');

        if ($pid) {
            $dailyTransaction = $this->dailyTransactionRepository->getDataByPID($pid);

            if ($dailyTransaction) {
                $date_now = $dailyTransaction['date'];
                $type = $dailyTransaction['type'];
            }
        }

        $data = $this->getDaysData($date_now, $type);

        if (!$data || !isset($data['data']) || !$data['data']) {
            return view('page.reports.index',
            [
                'pagename'    => 'Reports',
                'data'        => [],
                'report_type' => $report_type,
                'type'        => $type,
                'page_class'  => "reports",
            ]
        );
        }

        $settingsLogin = [];

        if (isset($data['settings'])) {
            $settingsLogin = $data['settings'];
        }

        $data['percentage'] = $this->getAverage($data['data'], $settingsLogin, $type . "_status");
        $data['class_status'] = $this->getStatuses($data['data'], $settingsLogin, $type . "_status");
        $columns = config('transaction_type.' . $type . '_status');
        $timeColumn = config('transaction_type.time_column');

        return view('page.reports.index',
            [
                'pagename'     => 'Reports',
                'data'         => $data,
                'report_type'  => $report_type,
                'type'         => $type,
                'columns'      => $columns,
                'time_column'  => $timeColumn,
                'page_class'   => "reports",
                'tab_selected' => $tab_selected,
            ]
        );
    }

    public function download($filename)
    {
        return Storage::download("public" . DIRECTORY_SEPARATOR .  $filename);
    }
}

