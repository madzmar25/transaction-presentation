<?php
namespace App\Traits\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
// use App\Http\Helpers\Facade\ExecutionTimerFacade as ExecutionTimer;
// use App\Transformers\DepartmentTransformer;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
trait Users
{
    /**
     * Get the list of users both active and inactive
     *
     * @return Json
     */
    public function list()
    {
        $data = User::get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                // $actionBtn = '<a href="' . route('user.edit', $row->pid) . '" data-toggle="modal" data-target="#edit-user" class="edit btn btn-success btn-sm btn-edit" data-edit="' . $row->pid . '">Edit</a> <a href="' . route('user.edit', $row->pid) . '"  data-toggle="modal" data-target="#delete-user" data-delete="' . $row->pid . '" class="delete btn btn-delete btn-danger btn-sm">Delete</a>';
                $actionBtn = '<a href="' . route('user.edit', $row->pid) . '" data-toggle="modal" data-target="#edit-user" class="edit btn btn-success btn-sm btn-edit" data-edit="' . $row->pid . '">Edit</a>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function deleteUser(Request $request, $user_id)
    {
        if (Hash::check($request->current_password, Auth::user()->password)) {
            $user = $this->userRepository->getDataById($user_id);

            if (!$user) {
                return response()->json(["success" => false, "message" => "Invalid user id, record not deleted!"], 301);
            }

            if ($this->userRepository->delete($user)) {
                return response()->json(["success" => true, "message" => "Record deleted succesfully!"], 200);
            }

            return response()->json(["success" => false, "message" => "Record not deleted!"], 301);
        } else {
            throw ValidationException::withMessages(['current_password' => 'Wrong current password']);
        }
    }

    public function updateUser(Request $request, $user_id)
    {
        if (Hash::check($request->current_password, Auth::user()->password)) {
            $user = $this->userRepository->getDataById($user_id);
            $data = $request->all();
            $data["id"] = $user->id;

            if ((isset($request['password']) && $request['password']) || (isset($request['password_confirmation']) && $request['password_confirmation'])) {
                $this->updateWithPasswordValidator($data)->validate();
            } else {
                $this->updateValidator($data)->validate();
            }

            if ($this->userRepository->update($user, $data)) {
                return response()->json(["success" => true, "message" => "Record updated succesfully!"], 200);
            }

            return response()->json(["success" => false, "message" => "Record not updated!"], 301);
        } else {
            throw ValidationException::withMessages(['current_password' => 'Wrong current password']);
        }
    }

    public function registerUser(Request $request)
    {

        if (Hash::check($request->current_password, Auth::user()->password)) {
            $request->request->add(['no_login' => true]);
            return $this->register($request);
        } else {
            throw ValidationException::withMessages(['current_password' => 'Wrong current password']);
        }
    }

    public function edit($user_id)
    {
        $user = $this->transform($this->userRepository->getDataById($user_id), true);
        $user['route'] = route('user.update', $user['pid']);
        unset($user['pid']);
        return response()->json(["data" =>$user, "success" => true, "message" => "Record found!"], 200);
    }
}

