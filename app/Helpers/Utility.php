<?php
namespace App\Helpers;

use App\Repositories\TransactionSettingRepository;
use DateTime;
/**
 * Trait UserTransformer
 *
 * This Trait will be responsible for transforming collection record
 * into a Prettier format to be presented as response
 */
class Utility
{
    public function toKeywordFormat($word)
    {
        return strtolower(str_replace(" ", "", $word));
    }

    public function toAlphaNumericOnly($word)
    {
        return strtolower(str_replace(["_", "-"," "], "", $word));
    }

    public function isValidType($type)
    {
        if ($type && ($type == TransactionSettingRepository::TRANSACTION || $type == TransactionSettingRepository::LOGIN)) {
            return true;
        }

        return false;
    }

    public function isLoginType($type)
    {
        return $type == TransactionSettingRepository::LOGIN;
    }

    public function getAverage($value1, $value2)
    {
        if ($value1 == 0) {
            $percentChange = $value2 * 100;
            return abs(round($percentChange, 2));
        }

        $percentChange = (1 - $value2 / $value1) * 100;
        return abs(round($percentChange, 2));
    }

    public function getStatusClass($value1, $value2)
    {
        if ($value1 > $value2) {
            return "higher";
        } elseif ($value1 < $value2) {
            return "lower";
        }

        return "none";
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    public function mapTimeToColumnName($time)
    {
        switch($time) {
            case "00":
                return "12am";
            case "01":
                return "1am";
            case "02":
                return "2am";
            case "03":
                return "3am";
            case "04":
                return "4am";
            case "05":
                return "5am";
            case "06":
                return "6am";
            case "07":
                return "7am";
            case "08":
                return "8am";
            case "09":
                return "9am";
            case "10":
                return "10am";
            case "11":
                return "11am";
            case "12":
                return "12pm";
            case "13":
                return "1pm";
            case "14":
                return "2pm";
            case "15":
                return "3pm";
            case "16":
                return "4pm";
            case "17":
                return "5pm";
            case "18":
                return "6pm";
            case "19":
                return "7pm";
            case "20":
                return "8pm";
            case "21":
                return "9pm";
            case "22":
                return "10pm";
            case "23":
                return "11pm";
        }
        return "";
    }
}
