<?php

namespace App\Imports;

use App\Models\LoginSetting;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Str;

class SettingsImport implements
    // ToCollection,
    // ToModel,
    WithUpserts,
    // WithHeadingRow,
    OnEachRow,
    SkipsEmptyRows
    // WithMappedCells,
    // WithStartRow
{
    use
    // RemembersRowNumber,
    SkipsErrors;
    private $is_peak;
    private $date;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function __construct(\Illuminate\Database\Eloquent\Model $model)
    {
        $this->currentModel = $model;
    }

    public function onRow(Row $row) {
        $rowIndex = $row->getIndex();
        $row      = $row->toArray();

        if ($rowIndex == 1) {
            $date = $row[1];
            $date_array = [];
            if ($date) {
                $date_array = explode(",", $date);
            }

            $this->date = $date_array;
        } elseif ($rowIndex == 2) {
            // If item is not Peak then set the date to the first day of month
            if (isset($row[1]) && (strtolower($row[1]) == "true" || strtolower($row[1]) == 1)) {
                $this->is_peak = 1;
            } else {
                $dates = $this->date;

                for($i = 0; $i < count($dates); $i++) {
                    $def_date = strtotime(trim($dates[$i]));
                    $this->date[$i] = date('Y-m-01', $def_date);
                }

                $this->is_peak = 0;
            }

        } elseif ($rowIndex >= 5) {
            $current_date = $this->date;

            foreach($current_date as $cdate) {
                $this->currentModel::updateOrCreate(
                    ['date' => $cdate, 'status' => $row[0], 'is_peak' => $this->is_peak, 'type' => $this->type],
                    [
                        'pid'     => (string) Str::uuid(),
                        'date'    => $cdate,
                        'type'    => $this->type,
                        'is_peak' => $this->is_peak,
                        'status'  => $row[0],
                        '12am'    => $row[1] ?? 0,
                        '1am'     => $row[2] ?? 0,
                        '2am'     => $row[3] ?? 0,
                        '3am'     => $row[4] ?? 0,
                        '4am'     => $row[5] ?? 0,
                        '5am'     => $row[6] ?? 0,
                        '6am'     => $row[7] ?? 0,
                        '7am'     => $row[8] ?? 0,
                        '8am'     => $row[9] ?? 0,
                        '9am'     => $row[10] ?? 0,
                        '10am'    => $row[11] ?? 0,
                        '11am'    => $row[12] ?? 0,
                        '12pm'    => $row[13] ?? 0,
                        '1pm'     => $row[14] ?? 0,
                        '2pm'     => $row[15] ?? 0,
                        '3pm'     => $row[16] ?? 0,
                        '4pm'     => $row[17] ?? 0,
                        '5pm'     => $row[18] ?? 0,
                        '6pm'     => $row[19] ?? 0,
                        '7pm'     => $row[20] ?? 0,
                        '8pm'     => $row[21] ?? 0,
                        '9pm'     => $row[22] ?? 0,
                        '10pm'    => $row[23] ?? 0,
                        '11pm'    => $row[24] ?? 0,
                    ]
                );
            }

        }
    }

    // public function headingRow(): int
    // {
    //     return 4;
    // }

    // public function startRow(): int
    // {
    //     return 5;
    // }

    // public function mapping(): array
    // {
    //     return [
    //         'date_from'  => 'B1',
    //         'date_to' => 'B2',
    //     ];
    // }

    public function uniqueBy()
    {
        return 'date';
    }
}
