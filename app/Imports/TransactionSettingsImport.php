<?php

namespace App\Imports;

use App\Models\TransactionSetting;

class TransactionSettingsImport extends SettingsImport
{
    public function __construct(TransactionSetting $model)
    {
        parent::__construct($model);
        $this->type = "transaction";
    }
}
