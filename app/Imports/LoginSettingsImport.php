<?php

namespace App\Imports;

use App\Models\TransactionSetting;

class LoginSettingsImport extends SettingsImport
{
    public function __construct(TransactionSetting $model)
    {
        parent::__construct($model);
        $this->type = "login";
    }
}
