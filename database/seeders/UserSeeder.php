<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'id'        => 1,
            'pid'       => '49eea5f9-7b14-42e6-b64e-736e3428ee35',
            'firstname' => "Admin",
            'lastname'  => 'Admin',
            'is_active' => 1,
            'email'     => 'admin@unionbankph.com',
            'password'  => Hash::make('password123')
        ]);
    }
}
