<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Helpers\UtilityFacade as Utility;
use DB;
use Illuminate\Support\Str;

class DailyTransactionSeeder extends Seeder
{
    public $month;
    public $status_t;
    public $day;
    public $id = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedTransaction("transaction");
        $this->seedLogin("login");
    }
    private function seedLogin($type)
    {
        $statuses = ['FAILED', 'PENDING', 'SUCCESS'];
        $to_insert = [];
        $j=$this->id;
        for($m= 1; $m <= 11; $m++) {
            for($i = 1; $i<=31; $i++) {
                if (!Utility::validateDate('2021-' . ($m < 10 ? '0' . $m : $m) . '-' . ($i < 10 ? '0' . $i : $i))) {
                    continue;
                }

                foreach($statuses as $status) {

                    $to_insert[] = [
                        'id'     => $j++,
                        'pid'    => (string) Str::uuid(),
                        'date'   => '2021-' . ($m < 10 ? '0' . $m : $m)  . '-' . ($i < 10 ? '0' . $i : $i),
                        'type'   => $type,
                        'status' => $status,
                        '12am'   => rand(rand(0, 100), 400),
                        '1am'    => rand(rand(0, 100), 400),
                        '2am'    => rand(rand(0, 100), 400),
                        '3am'    => rand(rand(0, 100), 400),
                        '4am'    => rand(rand(0, 100), 400),
                        '5am'    => rand(rand(0, 100), 400),
                        '6am'    => rand(rand(0, 100), 400),
                        '7am'    => rand(rand(0, 100), 400),
                        '8am'    => rand(rand(0, 100), 400),
                        '9am'    => rand(rand(0, 100), 400),
                        '10am'   => rand(rand(0, 100), 400),
                        '11am'   => rand(rand(0, 100), 400),
                        '12pm'   => rand(rand(0, 100), 400),
                        '1pm'    => rand(rand(0, 100), 400),
                        '2pm'    => rand(rand(0, 100), 400),
                        '3pm'    => rand(rand(0, 100), 400),
                        '4pm'    => rand(rand(0, 100), 400),
                        '5pm'    => rand(rand(0, 100), 400),
                        '6pm'    => rand(rand(0, 100), 400),
                        '7pm'    => rand(rand(0, 100), 400),
                        '8pm'    => rand(rand(0, 100), 400),
                        '9pm'    => rand(rand(0, 100), 400),
                        '10pm'   => rand(rand(0, 100), 400),
                        '11pm'   => rand(rand(0, 100), 400)
                    ];
                }

            }
        }
        \DB::table('daily_transactions')->insert($to_insert);
        $to_insert2 = [];

        foreach($statuses as $status) {

            $to_insert2[] =
            [
                'id'     => $j++,
                'pid'    => (string) Str::uuid(),
                'date'   => '2021-12-01',
                'type'   => $type,
                'status' => $status,
                '12am'   => rand(rand(0, 100), 400),
                '1am'    => rand(rand(0, 100), 400),
                '2am'    => rand(rand(0, 100), 400),
                '3am'    => rand(rand(0, 100), 400),
                '4am'    => rand(rand(0, 100), 400),
                '5am'    => rand(rand(0, 100), 400),
                '6am'    => rand(rand(0, 100), 400),
                '7am'    => rand(rand(0, 100), 400),
                '8am'    => 0,
                '9am'    => 0,
                '10am'   => 0,
                '11am'   => 0,
                '12pm'   => 0,
                '1pm'    => 0,
                '2pm'    => 0,
                '3pm'    => 0,
                '4pm'    => 0,
                '5pm'    => 0,
                '6pm'    => 0,
                '7pm'    => 0,
                '8pm'    => 0,
                '9pm'    => 0,
                '10pm'   => 0,
                '11pm'   => 0
            ];
        }

        \DB::table('daily_transactions')->insert($to_insert2);
        $this->id = $j;
    }
    private function seedTransaction($type)
    {
        $statuses = ['APPROVED', 'CANCELLED', 'CONDITIONAL', 'EXPIRED', 'FAILED', 'FOR_APPROVAL', 'FOR_OTP_VERIFICATION', 'FOR_PROCESSING', 'REJECTED', 'PROCESSING', 'RELEASED', 'SCHEDULED', 'SUCCESS'];

        $j=$this->id;
        for($m= 1; $m <= 11; $m++) {
            for($i = 1; $i<=31; $i++) {
                $to_insert = [];
                if (!Utility::validateDate('2021-' . ($m < 10 ? '0' . $m : $m) . '-' . ($i < 10 ? '0' . $i : $i))) {
                    continue;
                }

                foreach($statuses as $status) {
                    $to_insert[] = [
                        'id'     => $j++,
                        'pid'    => (string) Str::uuid(),
                        'date'   => '2021-' . ($m < 10 ? '0' . $m : $m)  . '-' . ($i < 10 ? '0' . $i : $i),
                        'type'   => $type,
                        'status' => $status,
                        '12am'   => rand(rand(0, 100), 400),
                        '1am'    => rand(rand(0, 100), 400),
                        '2am'    => rand(rand(0, 100), 400),
                        '3am'    => rand(rand(0, 100), 400),
                        '4am'    => rand(rand(0, 100), 400),
                        '5am'    => rand(rand(0, 100), 400),
                        '6am'    => rand(rand(0, 100), 400),
                        '7am'    => rand(rand(0, 100), 400),
                        '8am'    => rand(rand(0, 100), 400),
                        '9am'    => rand(rand(0, 100), 400),
                        '10am'   => rand(rand(0, 100), 400),
                        '11am'   => rand(rand(0, 100), 400),
                        '12pm'   => rand(rand(0, 100), 400),
                        '1pm'    => rand(rand(0, 100), 400),
                        '2pm'    => rand(rand(0, 100), 400),
                        '3pm'    => rand(rand(0, 100), 400),
                        '4pm'    => rand(rand(0, 100), 400),
                        '5pm'    => rand(rand(0, 100), 400),
                        '6pm'    => rand(rand(0, 100), 400),
                        '7pm'    => rand(rand(0, 100), 400),
                        '8pm'    => rand(rand(0, 100), 400),
                        '9pm'    => rand(rand(0, 100), 400),
                        '10pm'   => rand(rand(0, 100), 400),
                        '11pm'   => rand(rand(0, 100), 400)
                    ];
                }
                \DB::table('daily_transactions')->insert($to_insert);
            }
        }

        $to_insert2 = [];

        foreach($statuses as $status) {

            $to_insert2[] =
            [
                'id'     => $j++,
                'pid'    => (string) Str::uuid(),
                'date'   => '2021-12-01',
                'type'   => $type,
                'status' => $status,
                '12am'   => rand(rand(0, 100), 400),
                '1am'    => rand(rand(0, 100), 400),
                '2am'    => rand(rand(0, 100), 400),
                '3am'    => rand(rand(0, 100), 400),
                '4am'    => rand(rand(0, 100), 400),
                '5am'    => rand(rand(0, 100), 400),
                '6am'    => rand(rand(0, 100), 400),
                '7am'    => rand(rand(0, 100), 400),
                '8am'    => 0,
                '9am'    => 0,
                '10am'   => 0,
                '11am'   => 0,
                '12pm'   => 0,
                '1pm'    => 0,
                '2pm'    => 0,
                '3pm'    => 0,
                '4pm'    => 0,
                '5pm'    => 0,
                '6pm'    => 0,
                '7pm'    => 0,
                '8pm'    => 0,
                '9pm'    => 0,
                '10pm'   => 0,
                '11pm'   => 0
            ];
        }

        \DB::table('daily_transactions')->insert($to_insert2);
        $this->id = $j;
    }
}
