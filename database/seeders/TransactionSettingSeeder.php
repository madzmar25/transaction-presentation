<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Helpers\UtilityFacade as Utility;
use DB;


class TransactionSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['Transactions', 'Successful', 'Failed', 'Pending', 'For approval', 'Cancelled'];
        $num_of_peak = rand(1, 10);

        $to_insert = [];
        $id=1;
        for($month= 1; $month <= 11; $month++) {
            $days_of_month = [];
            $peak_days = [];
            $num_of_peak = rand(1, 10);

            for($i = 1; $i <= 31; $i++) {
                $days_of_month[] = $i;
            }

            for($j = $num_of_peak; $j >= 1; $j--)
            {
                shuffle($days_of_month);
                $peak_days[] = array_pop($days_of_month);
            }

            foreach($peak_days as $peak_day) {
                if (!Utility::validateDate('2021-' . ($month < 10 ? '0' . $month : $month) . '-' . ($peak_day < 10 ? '0' . $peak_day : $peak_day))) {
                    continue;
                }

                foreach($statuses as $status) {
                    $to_insert[] = [
                        'id'      => $id++,
                        'date'    => '2021-' . ($month < 10 ? '0' . $month : $month)  . '-' . ($peak_day < 10 ? '0' . $peak_day : $peak_day),
                        'is_peak' => 1,
                        'status'  => $status,
                        '12am'    => rand(0, 40),
                        '1am'     => rand(0, 40),
                        '2am'     => rand(0, 40),
                        '3am'     => rand(0, 40),
                        '4am'     => rand(0, 40),
                        '5am'     => rand(0, 40),
                        '6am'     => rand(0, 40),
                        '7am'     => rand(0, 40),
                        '8am'     => rand(0, 40),
                        '9am'     => rand(0, 40),
                        '10am'    => rand(0, 40),
                        '11am'    => rand(0, 40),
                        '12pm'    => rand(0, 40),
                        '1pm'     => rand(0, 40),
                        '2pm'     => rand(0, 40),
                        '3pm'     => rand(0, 40),
                        '4pm'     => rand(0, 40),
                        '5pm'     => rand(0, 40),
                        '6pm'     => rand(0, 40),
                        '7pm'     => rand(0, 40),
                        '8pm'     => rand(0, 40),
                        '9pm'     => rand(0, 40),
                        '10pm'    => rand(0, 40),
                        '11pm'    => rand(0, 40)
                    ];
                }

            }

            foreach($statuses as $status) {
                $to_insert[] =
                [
                    'id'      => $id++,
                    'date'    => '2021-'. ($month < 10 ? '0' . $month : $month)  . '-01',
                    'is_peak' => 0,
                    'status'  => $status,
                    '12am'    => rand(0, 40),
                    '1am'     => rand(0, 40),
                    '2am'     => rand(0, 40),
                    '3am'     => rand(0, 40),
                    '4am'     => rand(0, 40),
                    '5am'     => rand(0, 40),
                    '6am'     => rand(0, 40),
                    '7am'     => rand(0, 40),
                    '8am'     => rand(0, 40),
                    '9am'     => rand(0, 40),
                    '10am'    => rand(0, 40),
                    '11am'    => rand(0, 40),
                    '12pm'    => rand(0, 40),
                    '1pm'     => rand(0, 40),
                    '2pm'     => rand(0, 40),
                    '3pm'     => rand(0, 40),
                    '4pm'     => rand(0, 40),
                    '5pm'     => rand(0, 40),
                    '6pm'     => rand(0, 40),
                    '7pm'     => rand(0, 40),
                    '8pm'     => rand(0, 40),
                    '9pm'     => rand(0, 40),
                    '10pm'    => rand(0, 40),
                    '11pm'    => rand(0, 40)
                ];
            }
        }
        \DB::table('transaction_settings')->insert($to_insert);
    }
}
