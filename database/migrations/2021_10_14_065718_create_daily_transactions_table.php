<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pid');
            $table->date('date');
            $table->string('type');
            $table->string('status');
            $table->string('12am');
            $table->string('1am');
            $table->string('2am');
            $table->string('3am');
            $table->string('4am');
            $table->string('5am');
            $table->string('6am');
            $table->string('7am');
            $table->string('8am');
            $table->string('9am');
            $table->string('10am');
            $table->string('11am');
            $table->string('12pm');
            $table->string('1pm');
            $table->string('2pm');
            $table->string('3pm');
            $table->string('4pm');
            $table->string('5pm');
            $table->string('6pm');
            $table->string('7pm');
            $table->string('8pm');
            $table->string('9pm');
            $table->string('10pm');
            $table->string('11pm');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_transactions');
    }
}
