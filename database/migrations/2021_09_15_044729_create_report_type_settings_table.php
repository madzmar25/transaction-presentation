<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTypeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_type_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pid');
            $table->bigInteger('report_type_id')->unsigned();
            $table->date('date');
            $table->time('time');
            $table->string('status');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('report_type_id')
                ->references('id')->on('report_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_type_settings');
    }
}
