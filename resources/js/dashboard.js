$(function () {
    var updateLogin = (data, timeColumn) => {
        var percentages = data.percentage;
        var parents = $(".login-row");
        var dataVal = null;
        var dataSettings = null;

        if (data.hasOwnProperty('data')) {
            dataVal = data.data;
        }

        if (data.hasOwnProperty('settings')) {
            dataSettings = data.settings;
        }

        $.each(percentages, function(index, val) {
            var average = parents.find('h3 .' + index);
            average.html(val[timeColumn] + "<small>%</small>").addClass(index + " " + data.class_status[index][timeColumn]);
            if (dataVal && index in dataVal && timeColumn in dataVal[index]) {
                $("#current-login-" + index).html(dataVal[index][timeColumn]);
                $("#login-" + index + "-info").attr('href', '/reports/' + dataVal[index]["pid"] + "/" + index);
            }

            if (dataSettings && (index in dataSettings) && timeColumn in dataSettings[index]) {
                $("#average-login-" + index).html(dataSettings[index][timeColumn]);
            }
        });
    }

    var updateTransaction = (data, timeColumn) => {
        var percentages = data.percentage;
        var parents = $(".transaction-row");
        var dataVal = null;
        var dataSettings = null;

        if (data.hasOwnProperty('data')) {
            dataVal = data.data;
        }

        if (data.hasOwnProperty('settings')) {
            dataSettings = data.settings;
        }

        $.each(percentages, function(index, val) {
            var average = parents.find('.info-box-number.' + index);
            average.html(val[timeColumn] + "<small>%</small>").addClass(index + " info-box-number " + data.class_status[index][timeColumn]);

            if (dataVal && index in dataVal && timeColumn in dataVal[index]) {
                $("#current-transaction-" + index).html(dataVal[index][timeColumn]);
                $("#transaction-" + index + "-info").attr('href', '/reports/' + dataVal[index]["pid"] + "/" + index);
            }

            if (dataSettings && (index in dataSettings) && timeColumn in dataSettings[index]) {
                $("#average-transaction-" + index).html(dataSettings[index][timeColumn]);
            }
        });
    }

    var update = () => {
        $.ajax({
            type: 'GET',
            url: '/',
            dataType: 'json',
            success: function(response, textStatus, xhr) {
                console.log("Done running");
                if (response.hasOwnProperty('login')) {
                    updateLogin(response.login, response.columnTime);
                }

                if (response.hasOwnProperty('transaction')) {
                    updateTransaction(response.transaction, response.columnTime);
                }

                if (response.hasOwnProperty('users')) {
                    $("#login_user").html(response.users);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest, textStatus);
                alert("Unknown error encountered");
            }
        });
    }

    var updateTimer = () => {
        var dt = new Date();
        var seconds = dt.getSeconds();

        if (seconds == 0) {
            update();
        }

        var options = { timeZone: "Asia/Manila",
                        hour12: true,
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour: 'numeric',
                        minute: 'numeric'
                    };

        $(".timer").html(dt.toLocaleString('en-US', options));

    }

    if (window.Laravel.auth.user && $(".dashboard").length) {
        update();
        updateTimer();
        // setInterval(update, 60 * 1000);
        setInterval(updateTimer, 1000);
    }

    $(".more-info").on('click', function() {
        var target = $(this).data('target');
        var canvas = $(target);
        var chartParents = canvas.parents('.chart');
        var icons = $(this).find('i.fas');
        chartParents.fadeToggle(function() {
            if (icons.hasClass("fa-arrow-circle-up")) {
                icons.removeClass("fa-arrow-circle-up").addClass("fa-arrow-circle-down");
            } else {
                icons.removeClass("fa-arrow-circle-down").addClass("fa-arrow-circle-up");
            }
        });


        return false;
    });

});
