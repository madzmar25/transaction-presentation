function getData(elem, appendKey)
{
    return [
        elem.data(appendKey + "12am"),
        elem.data(appendKey + "1am"),
        elem.data(appendKey + "2am"),
        elem.data(appendKey + "3am"),
        elem.data(appendKey + "4am"),
        elem.data(appendKey + "5am"),
        elem.data(appendKey + "6am"),
        elem.data(appendKey + "7am"),
        elem.data(appendKey + "8am"),
        elem.data(appendKey + "9am"),
        elem.data(appendKey + "10am"),
        elem.data(appendKey + "11am"),
        elem.data(appendKey + "12pm"),
        elem.data(appendKey + "1pm"),
        elem.data(appendKey + "2pm"),
        elem.data(appendKey + "3pm"),
        elem.data(appendKey + "4pm"),
        elem.data(appendKey + "5pm"),
        elem.data(appendKey + "6pm"),
        elem.data(appendKey + "7pm"),
        elem.data(appendKey + "8pm"),
        elem.data(appendKey + "9pm"),
        elem.data(appendKey + "10pm"),
        elem.data(appendKey + "11pm")
    ];
}

function getSuccessOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "success-";
    }

    return {
        label               : 'Success',
        backgroundColor     : 'green',
        borderColor         : 'green',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'green',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'green',
        data                : getData(elem, key)
    };
}
function getReleasedOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "released-";
    }

    return {
        label               : 'Released',
        backgroundColor     : '#B2FFFF',
        borderColor         : '#B2FFFF',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : '#B2FFFF',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: '#B2FFFF',
        data                : getData(elem, key)
    };
}
function getScheduledOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "scheduled-";
    }

    return {
        label               : 'Scheduled',
        backgroundColor     : '#E4D00A',
        borderColor         : '#E4D00A',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : '#E4D00A',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: '#E4D00A',
        data                : getData(elem, key)
    };
}

function getRejectedOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "rejected-";
    }

    return {
        label               : 'Rejected',
        backgroundColor     : '#960018',
        borderColor         : '#960018',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : '#960018',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: '#960018',
        data                : getData(elem, key)
    };
}

function getForProcessingOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "forprocessing-";
    }

    return {
        label               : 'For Processing',
        backgroundColor     : '#536872',
        borderColor         : '#536872',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : '#536872',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: '#536872',
        data                : getData(elem, key)
    };
}

function getExpiredOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "expired-";
    }

    return {
        label               : 'Expired',
        backgroundColor     : 'pink',
        borderColor         : 'pink',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'pink',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'pink',
        data                : getData(elem, key)
    };
}

function getConditionalOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "conditional-";
    }

    return {
        label               : 'Conditional',
        backgroundColor     : 'apricot',
        borderColor         : 'apricot',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'apricot',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'apricot',
        data                : getData(elem, key)
    };
}

function getProcessingOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "processing-";
    }

    return {
        label               : 'Processing',
        backgroundColor     : 'black',
        borderColor         : 'black',
        pointRadius         : false,
        pointColor          : 'black',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'black',
        data                : getData(elem, key)
    };
}

function getForOtpVerificationOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "forotpverification-";
    }

    return {
        label               : 'For OTP Verification',
        backgroundColor     : 'yellow',
        borderColor         : 'yellow',
        pointRadius         : false,
        pointColor          : 'yellow',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'yellow',
        data                : getData(elem, key)
    };
}

function getForApprovalOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "forapproval-";
    }
    return {
        label               : 'For Approval',
        backgroundColor     : 'brown',
        borderColor         : 'brown',
        pointRadius         : false,
        pointColor          : 'brown',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'brown',
        data                : getData(elem, key)
    };
}

function getFailedOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "failed-";
    }
    return {
        label               : 'Failed',
        backgroundColor     : 'red',
        borderColor         : 'red',
        pointRadius         : false,
        pointColor          : 'red',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'red',
        data                : getData(elem, key)
    };
}

function getCancelledOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "cancelled-";
    }
    return {
        label               : 'Cancelled',
        backgroundColor     : 'orange',
        borderColor         : 'orange',
        pointRadius         : false,
        pointColor          : 'orange',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'orange',
        data                : getData(elem, key)
    };
}

function getApprovedOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "approved-";
    }
    return {
        label               : 'Approved',
        backgroundColor     : 'blue',
        borderColor         : 'blue',
        pointRadius         : false,
        pointColor          : 'blue',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'blue',
        data                : getData(elem, key)
    };
}

function getPendingOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "pending-";
    }
    return {
        label               : 'Pending',
        backgroundColor     : 'violet',
        borderColor         : 'violet',
        pointRadius         : false,
        pointColor          : 'violet',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'violet',
        data                : getData(elem, key)
    };
}

function getSettingsOpt(elem, withKey = true)
{
    var key = "";
    if (withKey) {
        key = "settings-";
    }
    return {
        label               : 'Average',
        backgroundColor     : '#ccc',
        borderColor         : '#ccc',
        pointRadius         : false,
        pointColor          : '#ccc',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: '#ccc',
        data                : getData(elem, key)
    };
}
function getChartLabel()
{
    return [
        '12am',
        '1am',
        '2am',
        '3am',
        '4am',
        '5am',
        '6am',
        '7am',
        '8am',
        '9am',
        '10am',
        '11am',
        '12pm',
        '1pm',
        '2pm',
        '3pm',
        '4pm',
        '5pm',
        '6pm',
        '7pm',
        '8pm',
        '9pm',
        '10pm',
        '11pm'
    ];
}


function assignChart(elem, type="transaction")
{
    var datasets = [];

    if (type == "transaction") {
        datasets = [
            getSuccessOpt(elem),
            getProcessingOpt(elem),
            getForOtpVerificationOpt(elem),
            getForApprovalOpt(elem),
            getFailedOpt(elem),
            getCancelledOpt(elem),
            getApprovedOpt(elem),
            getConditionalOpt(elem),
            getExpiredOpt(elem),
            getForProcessingOpt(elem),
            getRejectedOpt(elem),
            getReleasedOpt(elem),
            getScheduledOpt(elem),
        ];
    } else if (type == "login") {
        datasets = [
            getSuccessOpt(elem),
            getPendingOpt(elem),
            getFailedOpt(elem),
        ]
    }
    var areaChartData = {
        labels  : getChartLabel(),
        datasets: datasets
    }
    var barChartData = $.extend(true, {}, areaChartData)
    var barChartCanvas = $('#' + elem.attr('id')).get(0).getContext('2d')
    var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false,
        scales: {
            yAxes: [{
            ticks: {
                reverse: false,
                stepSize: 30
            },
            }]
        }
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    });
}

function getOpt(elem, target)
{
    switch(target) {
        case "success":
            return getSuccessOpt(elem, false);
        case "cancelled":
            return getCancelledOpt(elem, false);
        case "failed":
            return getFailedOpt(elem, false);
        case "forapproval":
            return getForApprovalOpt(elem, false);
        case "pending":
            return getPendingOpt(elem, false);
        case "forotpverification":
            return getForOtpVerificationOpt(elem, false);
        case "approved":
            return getApprovedOpt(elem, false);
        case "processing":
            return getProcessingOpt(elem, false);
        case "conditional":
            return getConditionalOpt(elem, false);
        case "expired":
            return getExpiredOpt(elem, false);
        case "forprocessing":
            return getForProcessingOpt(elem, false);
        case "rejected":
            return getRejectedOpt(elem, false);
        case "released":
            return getReleasedOpt(elem, false);
        case "scheduled":
            return getScheduledOpt(elem, false);
        default:
            return getSuccessOpt(elem, false);
    };
}

function assignIndividualChart(elem)
{
    var areaChartData = {
        labels  : getChartLabel(),
        datasets: [
            getOpt(elem, elem.data('target'))
        ]
    }
    //-------------
    //- BAR CHART -
    //-------------

    var barChartCanvas = $('#' + elem.attr('id')).get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    barChartData.datasets[0] = temp0

    var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false,
        scales: {
            yAxes: [{
            ticks: {
                reverse: false,
                stepSize: 30
            },
            }]
        }
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    });
}
function assignIndividualWithSettingsChart(elem)
{
    var areaChartData = {
        labels  : getChartLabel(),
        datasets: [
            getOpt(elem, elem.data('target')),
            getSettingsOpt(elem)
        ]
    }
    //-------------
    //- BAR CHART -
    //-------------

    var barChartCanvas = $('#' + elem.attr('id')).get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp0
    barChartData.datasets[1] = temp1

    var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false,
        scales: {
            yAxes: [{
            ticks: {
                reverse: false,
                stepSize: 30
            },
            }]
        }
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    });
}
$(function () {
    $all_chart_table = $(".all_chart_table");
    $chart_table = $(".chart_table");

    if ($all_chart_table.length) {
        $all_chart_table.each(function(index, element) {
            assignChart($(this));
        });
    }

    if ($chart_table.length) {
        $chart_table.each(function(index, element) {

            if ($(this).hasClass('has_settings')) {
                assignIndividualWithSettingsChart($(this));
            } else {
                assignIndividualChart($(this));
            }
        });
    }

    $('#search-daily-btn').on('click', function() {
        var selectedDate = $('#dailylogdate').find('.datetimepicker-input').val();
        var parentsForm = $(this).parents('form');
        var data = parentsForm.serialize();

        if (selectedDate == "") {
            return false;
        }

        $.ajax({
            type: parentsForm.attr('method'),
            url: parentsForm.attr('action'),
            data: data,
            dataType: 'json',
            beforeSend: function( xhr ) {
                $(".report-message-container").hide();
            },
            success: function(response) {
                $("#chart-container").html(response.view);
                $chart_table = $(".chart_table");
                if ($chart_table.length) {
                    $chart_table.each(function(index, element) {
                        if ($(this).hasClass('has_settings')) {
                            assignIndividualWithSettingsChart($(this));
                        } else {
                            assignIndividualChart($(this));
                        }
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                // We can set the view here right now it doesn't do with the current view.
                $("#chart-container").html(response.view);
                $(".report-message-container").html(response.message).addClass("alert alert-danger").fadeIn();
            }
        });
    });
    $('#search-btn').on('click', function() {
        var selectedDate = $('#reservationdate').find('.datetimepicker-input').val();
        var parentsForm = $(this).parents('form');
        var data = parentsForm.serialize();

        if (selectedDate == "") {
            return false;
        }

        $.ajax({
            type: parentsForm.attr('method'),
            url: parentsForm.attr('action'),
            data: data,
            dataType: 'json',
            success: function(response) {
                $("#tabs").html(response.view);
                $all_chart_table = $(".all_chart_table");
                $chart_table = $(".chart_table");

                if ($all_chart_table.length) {

                    $all_chart_table.each(function(index, element) {
                        var type = $(this).data('type');
                        assignChart($(this), type);
                    });
                }

                if ($chart_table.length) {
                    $chart_table.each(function(index, element) {
                        assignIndividualChart($(this));
                    });
                }
            }
        });
    });
});
