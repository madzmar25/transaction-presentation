$(function () {

    // if ($("#user_table").length) {
    //     getUserList("#user_table");
    // }
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "/user",
        columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex', 'visible': false},
            // {data: 'id', name: 'id'},
            {data: 'pid', name: 'pid'},
            {data: 'firstname', name: 'firstname'},
            {data: 'lastname', name: 'lastname'},

            {data: 'email', name: 'email'},
            {data: 'is_active', name: 'is_active'},
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [6]}
        ]
    });

    $("#save_user").on('click', function() {
        var form = $(this).parents('form');
        var data = form.serialize();

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: data,
            dataType: 'json',
            beforeSend: function( xhr ) {
            },
            success: function(data, textStatus, xhr) {
                console.log(data, textStatus, xhr);
                if (textStatus == "success") {
                    resetRegistrationForm(form, true);
                    $(".user-message-container").html("Record added successfully!").addClass("alert alert-success").fadeIn();
                    table.ajax.reload();

                } else {
                    $(".user-message-container").html("Failed to add new record").addClass("alert alert-danger").fadeIn();
                }
                $('#add-user').modal('toggle');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                $.each(response.errors, function(index, value) {
                    var item = $('input[name="' + index + '"]');
                    var is_array = Array.isArray(value);
                    item.addClass("is-invalid").after('<span class="invalid-feedback" role="alert">' +
                                                            '<strong>' + (is_array ? $(value).get(0) : value) + '</strong>' +
                                                        '</span>');
                });
            }
        });
    });

    $('#add-user').on('hidden.bs.modal', function () {
        resetRegistrationForm(form, true);
    });

    $(".yajra-datatable").on('click', '.btn-edit', function() {
        var href = $(this).attr('href');
        var form = $('#edit-user').find('form');

        $.ajax({
            type: 'GET',
            url: href,
            dataType: 'json',
            beforeSend: function( xhr ) {

            },
            success: function(data, textStatus, xhr) {
                resetRegistrationForm(form);
                if (textStatus == "success") {
                    var info = data.data;
                    var status = 0;

                    if(info.status.toLowerCase() == "active") {
                        status = 1;
                    }

                    form.find('#firstname').val(info.firstname);
                    form.find('#lastname').val(info.lastname);
                    form.find('#email').val(info.email);
                    form.find('#status').val(status);
                    form.attr('action', info.route);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                alert(textStatus);
            }
        });
        // return false;
    });

    $(".yajra-datatable").on('click', '.btn-delete', function() {
        var href = $(this).attr('href');
        var form = $('#delete-user').find('form');

        $.ajax({
            type: 'GET',
            url: href,
            dataType: 'json',
            beforeSend: function( xhr ) {

            },
            success: function(data, textStatus, xhr) {
                resetRegistrationForm(form);
                if (textStatus == "success") {
                    var info = data.data;
                    form.attr('action', info.route);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                alert(textStatus);
            }
        });
    });
    $("#delete_user").on('click', function() {
        var form = $(this).parents('form');
        var data = form.serialize();

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: data,
            dataType: 'json',
            beforeSend: function( xhr ) {
                $(".user-message-container").hide();
            },
            success: function(response, textStatus, xhr) {
                if (textStatus == "success") {
                    resetRegistrationForm(form, true);
                    table.ajax.reload();
                    $(".user-message-container").html(response.message).addClass("alert alert-success").fadeIn();
                } else {
                    $(".user-message-container").html(response.message).addClass("alert alert-danger").fadeIn();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                resetRegistrationForm(form);
                $.each(response.errors, function(index, value) {
                    var item = $('input[name="' + index + '"]');
                    var is_array = Array.isArray(value);
                    item.addClass("is-invalid").after('<span class="invalid-feedback" role="alert">' +
                                                            '<strong>' + (is_array ? $(value).get(0) : value) + '</strong>' +
                                                        '</span>');
                });
            }
        });
    });
    $("#update_user").on('click', function() {
        var form = $(this).parents('form');
        var data = form.serialize();

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: data,
            dataType: 'json',
            beforeSend: function( xhr ) {
                $(".user-message-container").hide();
            },
            success: function(response, textStatus, xhr) {
                if (textStatus == "success") {
                    resetRegistrationForm(form, true);
                    $(".user-message-container").html(response.message).addClass("alert alert-success").fadeIn();
                    table.ajax.reload();
                } else {
                    $(".user-message-container").html(response.message).addClass("alert alert-danger").fadeIn();
                }
                $('#edit-user').modal('toggle');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                resetRegistrationForm(form);
                $.each(response.errors, function(index, value) {
                    var item = $('input[name="' + index + '"]');
                    var is_array = Array.isArray(value);
                    item.addClass("is-invalid").after('<span class="invalid-feedback" role="alert">' +
                                                            '<strong>' + (is_array ? $(value).get(0) : value) + '</strong>' +
                                                        '</span>');
                });
            }
        });
    });

    $('#edit-user').on('hidden.bs.modal', function () {
        resetRegistrationForm(form), true;
    });
});

function resetRegistrationForm(form, clear_fields = false)
{
    form.find(".is-invalid").removeClass("is-invalid");
    form.find(".invalid-feedback").remove();

    if (clear_fields) {
        form[0].reset();
    }
}

function getUserList(tableIdentifier)
{
}
