
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en" class="<?= (isset($page_class)) ? $page_class : '' ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UnionBank | Transaction Presentation</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script>
         window.Laravel = {
            auth: {
                user: "{{ Auth::user() }}"
            }
        }
    </script>
</head>
<body class="hold-transition sidebar-mini @auth authenticated @endauth">
<div class="wrapper" id="app">
    @auth
    @include('common.menu')
  <!-- Main Sidebar Container -->
    @include('common.sidebar')
    @endauth
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @auth
    @include('common.header')
    @endauth
    <!-- /.content-header -->

    <!-- Main content -->

    @yield('content')

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @auth
    @include('common.footer')
  @endauth
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/Chart.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>

    <script>

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            });

            $('#reservationdate').datetimepicker({
                singleDatePicker: true,
                showDropdowns: true,
                format: 'MM/YYYY'
            });

            $('#dailylogdate').datetimepicker({
                singleDatePicker: true,
                showDropdowns: true,
                format: 'MM/DD/YYYY'
            });

            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'MM/DD/YYYY hh:mm A'
                }
            });


            $('#upload_file').on('change',function(e){
                var fileName = e.target.files[0].name;
                // var fileName = $(this).val();

                $(this).next('.custom-file-label').html(fileName);
            });

            $("#upload_btn").on('click', function(e) {
                e.preventDefault();
                var parent_form = $(this).parents('form');

                parent_form.submit();

                return false;
            });

        });
    </script>
</body>
</html>
