@extends('layouts.app')

@section('content')
<div class="login-container">
    <div class="background-image"></div>
    <div class="form-container">@include('auth.login-form')</div>
</div>
@endsection
