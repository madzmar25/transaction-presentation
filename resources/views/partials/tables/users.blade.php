<table id="user_table" class="table table-bordered table-striped" data-source="<?= route('user.list') ?>">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email Address</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email Address</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </tfoot>
</table>
