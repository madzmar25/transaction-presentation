@if(isset($data['failed']))
    <div class="chart">
        <canvas id="failedChart_{{ $key }}" class="chart_table"
            data-target="failed"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['failed']["date"]));
                $month = date("m", strtotime($data['failed']["date"]));
                $day   = date("d", strtotime($data['failed']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['failed']['12am'] }}"
            data-1am  = "{{ $data['failed']['1am'] }}"
            data-2am  = "{{ $data['failed']['2am'] }}"
            data-3am  = "{{ $data['failed']['3am'] }}"
            data-4am  = "{{ $data['failed']['4am'] }}"
            data-5am  = "{{ $data['failed']['5am'] }}"
            data-6am  = "{{ $data['failed']['6am'] }}"
            data-7am  = "{{ $data['failed']['7am'] }}"
            data-8am  = "{{ $data['failed']['8am'] }}"
            data-9am  = "{{ $data['failed']['9am'] }}"
            data-10am = "{{ $data['failed']['10am'] }}"
            data-11am = "{{ $data['failed']['11am'] }}"
            data-12pm = "{{ $data['failed']['12pm'] }}"
            data-1pm  = "{{ $data['failed']['1pm'] }}"
            data-2pm  = "{{ $data['failed']['2pm'] }}"
            data-3pm  = "{{ $data['failed']['3pm'] }}"
            data-4pm  = "{{ $data['failed']['4pm'] }}"
            data-5pm  = "{{ $data['failed']['5pm'] }}"
            data-6pm  = "{{ $data['failed']['6pm'] }}"
            data-7pm  = "{{ $data['failed']['7pm'] }}"
            data-8pm  = "{{ $data['failed']['8pm'] }}"
            data-9pm  = "{{ $data['failed']['9pm'] }}"
            data-10pm = "{{ $data['failed']['10pm'] }}"
            data-11pm = "{{ $data['failed']['11pm'] }}"
        ></canvas>
    </div>
@endif
