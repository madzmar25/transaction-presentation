@if(isset($data['rejected']))
    <div class="chart">
        <canvas id="rejectedChart_{{ $key }}" class="chart_table"
            data-target="rejected"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['rejected']["date"]));
                $month = date("m", strtotime($data['rejected']["date"]));
                $day   = date("d", strtotime($data['rejected']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['rejected']['12am'] }}"
            data-1am  = "{{ $data['rejected']['1am'] }}"
            data-2am  = "{{ $data['rejected']['2am'] }}"
            data-3am  = "{{ $data['rejected']['3am'] }}"
            data-4am  = "{{ $data['rejected']['4am'] }}"
            data-5am  = "{{ $data['rejected']['5am'] }}"
            data-6am  = "{{ $data['rejected']['6am'] }}"
            data-7am  = "{{ $data['rejected']['7am'] }}"
            data-8am  = "{{ $data['rejected']['8am'] }}"
            data-9am  = "{{ $data['rejected']['9am'] }}"
            data-10am = "{{ $data['rejected']['10am'] }}"
            data-11am = "{{ $data['rejected']['11am'] }}"
            data-12pm = "{{ $data['rejected']['12pm'] }}"
            data-1pm  = "{{ $data['rejected']['1pm'] }}"
            data-2pm  = "{{ $data['rejected']['2pm'] }}"
            data-3pm  = "{{ $data['rejected']['3pm'] }}"
            data-4pm  = "{{ $data['rejected']['4pm'] }}"
            data-5pm  = "{{ $data['rejected']['5pm'] }}"
            data-6pm  = "{{ $data['rejected']['6pm'] }}"
            data-7pm  = "{{ $data['rejected']['7pm'] }}"
            data-8pm  = "{{ $data['rejected']['8pm'] }}"
            data-9pm  = "{{ $data['rejected']['9pm'] }}"
            data-10pm = "{{ $data['rejected']['10pm'] }}"
            data-11pm = "{{ $data['rejected']['11pm'] }}"
        ></canvas>
    </div>
@endif
