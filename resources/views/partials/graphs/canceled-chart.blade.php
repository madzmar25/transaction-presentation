@if(isset($data['cancelled']))
    <div class="chart">
        <canvas id="canceledChart_{{ $key }}"
            class="chart_table"
            data-target="cancelled"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year = date("Y", strtotime($data['cancelled']["date"]));
                $month = date("m", strtotime($data['cancelled']["date"]));
                $day = date("d", strtotime($data['cancelled']["date"]));
            @endphp
            data-year           = "<?= $year ?>"
            data-month          = "<?= $month ?>"
            data-day            = "<?= $day ?>"
            data-12am = "{{ $data['cancelled']['12am'] }}"
            data-1am  = "{{ $data['cancelled']['1am'] }}"
            data-2am  = "{{ $data['cancelled']['2am'] }}"
            data-3am  = "{{ $data['cancelled']['3am'] }}"
            data-4am  = "{{ $data['cancelled']['4am'] }}"
            data-5am  = "{{ $data['cancelled']['5am'] }}"
            data-6am  = "{{ $data['cancelled']['6am'] }}"
            data-7am  = "{{ $data['cancelled']['7am'] }}"
            data-8am  = "{{ $data['cancelled']['8am'] }}"
            data-9am  = "{{ $data['cancelled']['9am'] }}"
            data-10am = "{{ $data['cancelled']['10am'] }}"
            data-11am = "{{ $data['cancelled']['11am'] }}"
            data-12pm = "{{ $data['cancelled']['12pm'] }}"
            data-1pm  = "{{ $data['cancelled']['1pm'] }}"
            data-2pm  = "{{ $data['cancelled']['2pm'] }}"
            data-3pm  = "{{ $data['cancelled']['3pm'] }}"
            data-4pm  = "{{ $data['cancelled']['4pm'] }}"
            data-5pm  = "{{ $data['cancelled']['5pm'] }}"
            data-6pm  = "{{ $data['cancelled']['6pm'] }}"
            data-7pm  = "{{ $data['cancelled']['7pm'] }}"
            data-8pm  = "{{ $data['cancelled']['8pm'] }}"
            data-9pm  = "{{ $data['cancelled']['9pm'] }}"
            data-10pm = "{{ $data['cancelled']['10pm'] }}"
            data-11pm = "{{ $data['cancelled']['11pm'] }}">
        </canvas>
    </div>
@endif
