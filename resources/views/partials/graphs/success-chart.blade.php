@if(isset($data['success']))
    <div class="chart">
        <canvas id="successChart_{{ $key }}" class="chart_table"
            data-target="success"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['success']["date"]));
                $month = date("m", strtotime($data['success']["date"]));
                $day   = date("d", strtotime($data['success']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['success']['12am'] }}"
            data-1am  = "{{ $data['success']['1am'] }}"
            data-2am  = "{{ $data['success']['2am'] }}"
            data-3am  = "{{ $data['success']['3am'] }}"
            data-4am  = "{{ $data['success']['4am'] }}"
            data-5am  = "{{ $data['success']['5am'] }}"
            data-6am  = "{{ $data['success']['6am'] }}"
            data-7am  = "{{ $data['success']['7am'] }}"
            data-8am  = "{{ $data['success']['8am'] }}"
            data-9am  = "{{ $data['success']['9am'] }}"
            data-10am = "{{ $data['success']['10am'] }}"
            data-11am = "{{ $data['success']['11am'] }}"
            data-12pm = "{{ $data['success']['12pm'] }}"
            data-1pm  = "{{ $data['success']['1pm'] }}"
            data-2pm  = "{{ $data['success']['2pm'] }}"
            data-3pm  = "{{ $data['success']['3pm'] }}"
            data-4pm  = "{{ $data['success']['4pm'] }}"
            data-5pm  = "{{ $data['success']['5pm'] }}"
            data-6pm  = "{{ $data['success']['6pm'] }}"
            data-7pm  = "{{ $data['success']['7pm'] }}"
            data-8pm  = "{{ $data['success']['8pm'] }}"
            data-9pm  = "{{ $data['success']['9pm'] }}"
            data-10pm = "{{ $data['success']['10pm'] }}"
            data-11pm = "{{ $data['success']['11pm'] }}"
        ></canvas>
    </div>
@endif
