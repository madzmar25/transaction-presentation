@if(isset($data['for_otp_verification']))
    <div class="chart">
        <canvas id="forOtpVerificationChart_{{ $key }}" class="chart_table"
            data-target="forotpverification"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['for_otp_verification']["date"]));
                $month = date("m", strtotime($data['for_otp_verification']["date"]));
                $day   = date("d", strtotime($data['for_otp_verification']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['for_otp_verification']['12am'] }}"
            data-1am  = "{{ $data['for_otp_verification']['1am'] }}"
            data-2am  = "{{ $data['for_otp_verification']['2am'] }}"
            data-3am  = "{{ $data['for_otp_verification']['3am'] }}"
            data-4am  = "{{ $data['for_otp_verification']['4am'] }}"
            data-5am  = "{{ $data['for_otp_verification']['5am'] }}"
            data-6am  = "{{ $data['for_otp_verification']['6am'] }}"
            data-7am  = "{{ $data['for_otp_verification']['7am'] }}"
            data-8am  = "{{ $data['for_otp_verification']['8am'] }}"
            data-9am  = "{{ $data['for_otp_verification']['9am'] }}"
            data-10am = "{{ $data['for_otp_verification']['10am'] }}"
            data-11am = "{{ $data['for_otp_verification']['11am'] }}"
            data-12pm = "{{ $data['for_otp_verification']['12pm'] }}"
            data-1pm  = "{{ $data['for_otp_verification']['1pm'] }}"
            data-2pm  = "{{ $data['for_otp_verification']['2pm'] }}"
            data-3pm  = "{{ $data['for_otp_verification']['3pm'] }}"
            data-4pm  = "{{ $data['for_otp_verification']['4pm'] }}"
            data-5pm  = "{{ $data['for_otp_verification']['5pm'] }}"
            data-6pm  = "{{ $data['for_otp_verification']['6pm'] }}"
            data-7pm  = "{{ $data['for_otp_verification']['7pm'] }}"
            data-8pm  = "{{ $data['for_otp_verification']['8pm'] }}"
            data-9pm  = "{{ $data['for_otp_verification']['9pm'] }}"
            data-10pm = "{{ $data['for_otp_verification']['10pm'] }}"
            data-11pm = "{{ $data['for_otp_verification']['11pm'] }}"
        ></canvas>
    </div>
@endif
