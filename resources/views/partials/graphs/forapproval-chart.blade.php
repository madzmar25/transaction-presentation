@if(isset($data['for_approval']))
    <div class="chart">
        <canvas id="forApprovalChart_{{ $key }}" class="chart_table"
            data-target="forapproval"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['for_approval']["date"]));
                $month = date("m", strtotime($data['for_approval']["date"]));
                $day   = date("d", strtotime($data['for_approval']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['for_approval']['12am'] }}"
            data-1am  = "{{ $data['for_approval']['1am'] }}"
            data-2am  = "{{ $data['for_approval']['2am'] }}"
            data-3am  = "{{ $data['for_approval']['3am'] }}"
            data-4am  = "{{ $data['for_approval']['4am'] }}"
            data-5am  = "{{ $data['for_approval']['5am'] }}"
            data-6am  = "{{ $data['for_approval']['6am'] }}"
            data-7am  = "{{ $data['for_approval']['7am'] }}"
            data-8am  = "{{ $data['for_approval']['8am'] }}"
            data-9am  = "{{ $data['for_approval']['9am'] }}"
            data-10am = "{{ $data['for_approval']['10am'] }}"
            data-11am = "{{ $data['for_approval']['11am'] }}"
            data-12pm = "{{ $data['for_approval']['12pm'] }}"
            data-1pm  = "{{ $data['for_approval']['1pm'] }}"
            data-2pm  = "{{ $data['for_approval']['2pm'] }}"
            data-3pm  = "{{ $data['for_approval']['3pm'] }}"
            data-4pm  = "{{ $data['for_approval']['4pm'] }}"
            data-5pm  = "{{ $data['for_approval']['5pm'] }}"
            data-6pm  = "{{ $data['for_approval']['6pm'] }}"
            data-7pm  = "{{ $data['for_approval']['7pm'] }}"
            data-8pm  = "{{ $data['for_approval']['8pm'] }}"
            data-9pm  = "{{ $data['for_approval']['9pm'] }}"
            data-10pm = "{{ $data['for_approval']['10pm'] }}"
            data-11pm = "{{ $data['for_approval']['11pm'] }}"
        ></canvas>
    </div>
@endif
