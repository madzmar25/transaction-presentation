<div class="form-group row">

    <div class="col-md-4">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('Status') }}</label>
            <div class="col-sm-10">
                <select class="form-control select2bs4" style="width: 100%;">
                    <option selected="selected" value="">--- {{ __('Select Report') }} ---</option>
                    <?php $reports = config('report-type'); ?>
                    <?php /*foreach($reports as $index => $report_label):*/ ?>
                        <option value="">{{ __('Select Status') }}</option>
                        <option value="">{{ __('Transaction') }}</option>
                        <option value="">{{ __('Processing') }}</option>
                        <option value="">{{ __('Failed') }}</option>
                        <option value="">{{ __('Success') }}</option>
                    <?php /*endforeach;*/ ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <label for="field1" class="col-sm-4 col-form-label">{{ __('Date and time range:') }}</label>
            <div class="col-sm-8 input-group date" id="reservationdate" data-target-input="nearest">
                <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                <!--<input type="text" class="form-control float-right" id="reservationtime">-->
                <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="icheck-success d-inline">
            <input type="checkbox" checked id="checkboxSuccess1">
            <label for="checkboxSuccess1">
                {{ __('Set as Peak Day') }}
            </label>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('12:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('12:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('01:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('01:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('02:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('02:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('03:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('03:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('04:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('04:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('05:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('05:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('06:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('06:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('07:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('07:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('08:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('08:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('09:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('09:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('10:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('10:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        <div class="row">
            <label for="field1" class="col-sm-2 col-form-label">{{ __('11:00 am') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field1" placeholder="Field 1">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <label for="field2" class="col-sm-2 col-form-label">{{ __('11:00 pm') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="field2" placeholder="Field 2">
            </div>
        </div>
    </div>
</div>
<div class="float-right">
    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit">{{ __('Edit') }} <i class="fas fa-edit"></i></button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#save">{{ __('Save') }} <i class="fas fa-save"></i></button>
</div>
