@if(isset($data['approved']))
    <div class="chart">
        <canvas id="approvedChart_<?= $key ?>" class="chart_table"
            data-target="approved"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['approved']["date"]));
                $month = date("m", strtotime($data['approved']["date"]));
                $day   = date("d", strtotime($data['approved']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['approved']['12am'] }}"
            data-1am  = "{{ $data['approved']['1am'] }}"
            data-2am  = "{{ $data['approved']['2am'] }}"
            data-3am  = "{{ $data['approved']['3am'] }}"
            data-4am  = "{{ $data['approved']['4am'] }}"
            data-5am  = "{{ $data['approved']['5am'] }}"
            data-6am  = "{{ $data['approved']['6am'] }}"
            data-7am  = "{{ $data['approved']['7am'] }}"
            data-8am  = "{{ $data['approved']['8am'] }}"
            data-9am  = "{{ $data['approved']['9am'] }}"
            data-10am = "{{ $data['approved']['10am'] }}"
            data-11am = "{{ $data['approved']['11am'] }}"
            data-12pm = "{{ $data['approved']['12pm'] }}"
            data-1pm  = "{{ $data['approved']['1pm'] }}"
            data-2pm  = "{{ $data['approved']['2pm'] }}"
            data-3pm  = "{{ $data['approved']['3pm'] }}"
            data-4pm  = "{{ $data['approved']['4pm'] }}"
            data-5pm  = "{{ $data['approved']['5pm'] }}"
            data-6pm  = "{{ $data['approved']['6pm'] }}"
            data-7pm  = "{{ $data['approved']['7pm'] }}"
            data-8pm  = "{{ $data['approved']['8pm'] }}"
            data-9pm  = "{{ $data['approved']['9pm'] }}"
            data-10pm = "{{ $data['approved']['10pm'] }}"
            data-11pm = "{{ $data['approved']['11pm'] }}"
        ></canvas>
    </div>
@endif
