@if(isset($data['scheduled']))
    <div class="chart">
        <canvas id="scheduledChart_{{ $key }}" class="chart_table"
            data-target="scheduled"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['scheduled']["date"]));
                $month = date("m", strtotime($data['scheduled']["date"]));
                $day   = date("d", strtotime($data['scheduled']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['scheduled']['12am'] }}"
            data-1am  = "{{ $data['scheduled']['1am'] }}"
            data-2am  = "{{ $data['scheduled']['2am'] }}"
            data-3am  = "{{ $data['scheduled']['3am'] }}"
            data-4am  = "{{ $data['scheduled']['4am'] }}"
            data-5am  = "{{ $data['scheduled']['5am'] }}"
            data-6am  = "{{ $data['scheduled']['6am'] }}"
            data-7am  = "{{ $data['scheduled']['7am'] }}"
            data-8am  = "{{ $data['scheduled']['8am'] }}"
            data-9am  = "{{ $data['scheduled']['9am'] }}"
            data-10am = "{{ $data['scheduled']['10am'] }}"
            data-11am = "{{ $data['scheduled']['11am'] }}"
            data-12pm = "{{ $data['scheduled']['12pm'] }}"
            data-1pm  = "{{ $data['scheduled']['1pm'] }}"
            data-2pm  = "{{ $data['scheduled']['2pm'] }}"
            data-3pm  = "{{ $data['scheduled']['3pm'] }}"
            data-4pm  = "{{ $data['scheduled']['4pm'] }}"
            data-5pm  = "{{ $data['scheduled']['5pm'] }}"
            data-6pm  = "{{ $data['scheduled']['6pm'] }}"
            data-7pm  = "{{ $data['scheduled']['7pm'] }}"
            data-8pm  = "{{ $data['scheduled']['8pm'] }}"
            data-9pm  = "{{ $data['scheduled']['9pm'] }}"
            data-10pm = "{{ $data['scheduled']['10pm'] }}"
            data-11pm = "{{ $data['scheduled']['11pm'] }}"
        ></canvas>
    </div>
@endif
