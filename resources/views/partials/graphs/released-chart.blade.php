@if(isset($data['released']))
    <div class="chart">
        <canvas id="releasedChart_{{ $key }}" class="chart_table"
            data-target="released"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['released']["date"]));
                $month = date("m", strtotime($data['released']["date"]));
                $day   = date("d", strtotime($data['released']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['released']['12am'] }}"
            data-1am  = "{{ $data['released']['1am'] }}"
            data-2am  = "{{ $data['released']['2am'] }}"
            data-3am  = "{{ $data['released']['3am'] }}"
            data-4am  = "{{ $data['released']['4am'] }}"
            data-5am  = "{{ $data['released']['5am'] }}"
            data-6am  = "{{ $data['released']['6am'] }}"
            data-7am  = "{{ $data['released']['7am'] }}"
            data-8am  = "{{ $data['released']['8am'] }}"
            data-9am  = "{{ $data['released']['9am'] }}"
            data-10am = "{{ $data['released']['10am'] }}"
            data-11am = "{{ $data['released']['11am'] }}"
            data-12pm = "{{ $data['released']['12pm'] }}"
            data-1pm  = "{{ $data['released']['1pm'] }}"
            data-2pm  = "{{ $data['released']['2pm'] }}"
            data-3pm  = "{{ $data['released']['3pm'] }}"
            data-4pm  = "{{ $data['released']['4pm'] }}"
            data-5pm  = "{{ $data['released']['5pm'] }}"
            data-6pm  = "{{ $data['released']['6pm'] }}"
            data-7pm  = "{{ $data['released']['7pm'] }}"
            data-8pm  = "{{ $data['released']['8pm'] }}"
            data-9pm  = "{{ $data['released']['9pm'] }}"
            data-10pm = "{{ $data['released']['10pm'] }}"
            data-11pm = "{{ $data['released']['11pm'] }}"
        ></canvas>
    </div>
@endif
