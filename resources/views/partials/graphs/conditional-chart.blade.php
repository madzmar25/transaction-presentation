@if(isset($data['conditional']))
    <div class="chart">
        <canvas id="conditionalChart_{{ $key }}" class="chart_table"
            data-target="conditional"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['conditional']["date"]));
                $month = date("m", strtotime($data['conditional']["date"]));
                $day   = date("d", strtotime($data['conditional']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['conditional']['12am'] }}"
            data-1am  = "{{ $data['conditional']['1am'] }}"
            data-2am  = "{{ $data['conditional']['2am'] }}"
            data-3am  = "{{ $data['conditional']['3am'] }}"
            data-4am  = "{{ $data['conditional']['4am'] }}"
            data-5am  = "{{ $data['conditional']['5am'] }}"
            data-6am  = "{{ $data['conditional']['6am'] }}"
            data-7am  = "{{ $data['conditional']['7am'] }}"
            data-8am  = "{{ $data['conditional']['8am'] }}"
            data-9am  = "{{ $data['conditional']['9am'] }}"
            data-10am = "{{ $data['conditional']['10am'] }}"
            data-11am = "{{ $data['conditional']['11am'] }}"
            data-12pm = "{{ $data['conditional']['12pm'] }}"
            data-1pm  = "{{ $data['conditional']['1pm'] }}"
            data-2pm  = "{{ $data['conditional']['2pm'] }}"
            data-3pm  = "{{ $data['conditional']['3pm'] }}"
            data-4pm  = "{{ $data['conditional']['4pm'] }}"
            data-5pm  = "{{ $data['conditional']['5pm'] }}"
            data-6pm  = "{{ $data['conditional']['6pm'] }}"
            data-7pm  = "{{ $data['conditional']['7pm'] }}"
            data-8pm  = "{{ $data['conditional']['8pm'] }}"
            data-9pm  = "{{ $data['conditional']['9pm'] }}"
            data-10pm = "{{ $data['conditional']['10pm'] }}"
            data-11pm = "{{ $data['conditional']['11pm'] }}"
        ></canvas>
    </div>
@endif
