@if(isset($data['processing']))
    <div class="chart">
        <canvas id="processingChart_{{ $key }}" class="chart_table"
            data-target="processing"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year = date("Y", strtotime($data['processing']["date"]));
                $month = date("m", strtotime($data['processing']["date"]));
                $day = date("d", strtotime($data['processing']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['processing']['12am'] }}"
            data-1am  = "{{ $data['processing']['1am'] }}"
            data-2am  = "{{ $data['processing']['2am'] }}"
            data-3am  = "{{ $data['processing']['3am'] }}"
            data-4am  = "{{ $data['processing']['4am'] }}"
            data-5am  = "{{ $data['processing']['5am'] }}"
            data-6am  = "{{ $data['processing']['6am'] }}"
            data-7am  = "{{ $data['processing']['7am'] }}"
            data-8am  = "{{ $data['processing']['8am'] }}"
            data-9am  = "{{ $data['processing']['9am'] }}"
            data-10am = "{{ $data['processing']['10am'] }}"
            data-11am = "{{ $data['processing']['11am'] }}"
            data-12pm = "{{ $data['processing']['12pm'] }}"
            data-1pm  = "{{ $data['processing']['1pm'] }}"
            data-2pm  = "{{ $data['processing']['2pm'] }}"
            data-3pm  = "{{ $data['processing']['3pm'] }}"
            data-4pm  = "{{ $data['processing']['4pm'] }}"
            data-5pm  = "{{ $data['processing']['5pm'] }}"
            data-6pm  = "{{ $data['processing']['6pm'] }}"
            data-7pm  = "{{ $data['processing']['7pm'] }}"
            data-8pm  = "{{ $data['processing']['8pm'] }}"
            data-9pm  = "{{ $data['processing']['9pm'] }}"
            data-10pm = "{{ $data['processing']['10pm'] }}"
            data-11pm = "{{ $data['processing']['11pm'] }}"
        ></canvas>
    </div>
@endif
