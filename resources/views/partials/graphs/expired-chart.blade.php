@if(isset($data['expired']))
    <div class="chart">
        <canvas id="expiredChart_{{ $key }}" class="chart_table"
            data-target="expired"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['expired']["date"]));
                $month = date("m", strtotime($data['expired']["date"]));
                $day   = date("d", strtotime($data['expired']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['expired']['12am'] }}"
            data-1am  = "{{ $data['expired']['1am'] }}"
            data-2am  = "{{ $data['expired']['2am'] }}"
            data-3am  = "{{ $data['expired']['3am'] }}"
            data-4am  = "{{ $data['expired']['4am'] }}"
            data-5am  = "{{ $data['expired']['5am'] }}"
            data-6am  = "{{ $data['expired']['6am'] }}"
            data-7am  = "{{ $data['expired']['7am'] }}"
            data-8am  = "{{ $data['expired']['8am'] }}"
            data-9am  = "{{ $data['expired']['9am'] }}"
            data-10am = "{{ $data['expired']['10am'] }}"
            data-11am = "{{ $data['expired']['11am'] }}"
            data-12pm = "{{ $data['expired']['12pm'] }}"
            data-1pm  = "{{ $data['expired']['1pm'] }}"
            data-2pm  = "{{ $data['expired']['2pm'] }}"
            data-3pm  = "{{ $data['expired']['3pm'] }}"
            data-4pm  = "{{ $data['expired']['4pm'] }}"
            data-5pm  = "{{ $data['expired']['5pm'] }}"
            data-6pm  = "{{ $data['expired']['6pm'] }}"
            data-7pm  = "{{ $data['expired']['7pm'] }}"
            data-8pm  = "{{ $data['expired']['8pm'] }}"
            data-9pm  = "{{ $data['expired']['9pm'] }}"
            data-10pm = "{{ $data['expired']['10pm'] }}"
            data-11pm = "{{ $data['expired']['11pm'] }}"
        ></canvas>
    </div>
@endif
