<div class="form-group row">
    <div class="col-md-5">
        <label>Report Type</label>
        <select class="form-control select2bs4" style="width: 100%;">
            <option selected="selected" value="">--- {{ __('Select Report') }} ---</option>
            @php $reports = config('report-type') @endphp
            @foreach($reports as $index => $report_label)
                <option value="<?= $index ?>"><?= $report_label ?></option>
            @endforeach
        </select>

    </div>
    <div class="col-md-6">
        <label>Date and time range:</label>
        <div class="input-group date" id="reservationdate" data-target-input="nearest">

            <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
            <!--<input type="text" class="form-control float-right" id="reservationtime">-->
            <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
        </div>
        <!--
        <div class="input-group date" id="reservationdate" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
        -->
    </div>
    <div class="col-md-1 d-flex align-items-end">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#search">{{ __('Search') }} <i class="fas fa-search"></i></button>
    </div>
</div>
