
@if(isset($data['pending']))
    <div class="chart">
        <canvas id="pendingChart_{{ $key }}" class="chart_table"
            data-target="pending"
            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
            @php
                $year  = date("Y", strtotime($data['pending']["date"]));
                $month = date("m", strtotime($data['pending']["date"]));
                $day   = date("d", strtotime($data['pending']["date"]));
            @endphp
            data-year              = "{{ $year }}"
            data-month             = "{{ $month }}"
            data-day               = "{{ $day }}"
            data-12am = "{{ $data['pending']['12am'] }}"
            data-1am  = "{{ $data['pending']['1am'] }}"
            data-2am  = "{{ $data['pending']['2am'] }}"
            data-3am  = "{{ $data['pending']['3am'] }}"
            data-4am  = "{{ $data['pending']['4am'] }}"
            data-5am  = "{{ $data['pending']['5am'] }}"
            data-6am  = "{{ $data['pending']['6am'] }}"
            data-7am  = "{{ $data['pending']['7am'] }}"
            data-8am  = "{{ $data['pending']['8am'] }}"
            data-9am  = "{{ $data['pending']['9am'] }}"
            data-10am = "{{ $data['pending']['10am'] }}"
            data-11am = "{{ $data['pending']['11am'] }}"
            data-12pm = "{{ $data['pending']['12pm'] }}"
            data-1pm  = "{{ $data['pending']['1pm'] }}"
            data-2pm  = "{{ $data['pending']['2pm'] }}"
            data-3pm  = "{{ $data['pending']['3pm'] }}"
            data-4pm  = "{{ $data['pending']['4pm'] }}"
            data-5pm  = "{{ $data['pending']['5pm'] }}"
            data-6pm  = "{{ $data['pending']['6pm'] }}"
            data-7pm  = "{{ $data['pending']['7pm'] }}"
            data-8pm  = "{{ $data['pending']['8pm'] }}"
            data-9pm  = "{{ $data['pending']['9pm'] }}"
            data-10pm = "{{ $data['pending']['10pm'] }}"
            data-11pm = "{{ $data['pending']['11pm'] }}"
        ></canvas>
    </div>
@endif
