<ul>
    <li><a href="{{ route('template.download', 'transaction_template.xls') }}">{{ __('Download Transaction Template') }}</a></li>
    <li><a href="{{ route('template.download', 'login_template.xls') }}">{{ __('Download Login Template') }}</a></li>
</ul>
