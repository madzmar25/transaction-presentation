<div class="row">
    <div class="col-md-12">
        @include('partials.graphs.chart', ['key'=> $key, 'data' => $value, 'type' => 'login'])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.success-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.pending-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.failed-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
