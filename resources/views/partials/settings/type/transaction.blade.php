<div class="row">
    <div class="col-md-12">
        @include('partials.graphs.chart', ['key'=> $key, 'data' => $value, 'type' => 'transaction'])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.processing-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.for-processing-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.success-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.failed-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.forapproval-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.approved-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.rejected-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.canceled-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.scheduled-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.released-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.expired-chart', ['key'=> $key, 'data' => $value])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.conditional-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.for-otp-verification-chart', ['key'=> $key, 'data' => $value])
    </div>
</div>
