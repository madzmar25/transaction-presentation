<form class="form-inline" action="{{ route('setting.transaction.search') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="field1" class="col-form-label">{{ __('Date and time range:') }}</label>
        <div class="input-group date ml-3" id="reservationdate" data-target-input="nearest">
            <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
            <!--<input type="text" class="form-control float-right" id="reservationtime">-->
            <input type="text" class="form-control datetimepicker-input" name="date" data-target="#reservationdate"/>
        </div>
        <div class="input-group ml-3">
            <select class="form-control" name="report_type" id="report_type" aria-label="select">
                <option value="" selected>- Select type -</option>
                @foreach ($report_type as $key => $type)
                <option value="<?= $key ?>"><?= $type ?></option>
                @endforeach
              </select>
        </div>
        <button type="button" id="search-btn" class="btn btn-success ml-3">{{ __('Search') }}</button>
    </div>
</form>
