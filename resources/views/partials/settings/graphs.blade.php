@if($tabs)
<div class="card card-info card-tabs">
    <div class="card-header p-0 pt-1">
      <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
        @php $activeSelected = true; @endphp
        @foreach ($tabs as $key => $value)
        <li class="nav-item">
            <a class="nav-link {{ $activeSelected ? 'active' : '' }}"
                id="custom-tabs-{{ $key }}-tab"
                data-toggle="pill"
                href="#custom-tabs-{{ $key }}"
                role="tab"
                aria-controls="custom-tabs-{{ $key }}"
                aria-selected="true">
                    {{ $key }} {{ (isset($value['is_peak']) && $value['is_peak'] ? "[Peak]" : "[Default]") }}
            </a>
        </li>
            @php
                if($activeSelected) {
                    $activeSelected = false;
                }
            @endphp
        @endforeach
      </ul>
    </div>

    <div class="card-body">
      <div class="tab-content" id="custom-tabs-one-tabContent">
        @php $activeSelected = true; @endphp
        @foreach ($tabs as $key => $value)
            <div class="tab-pane fade {{ $activeSelected ? 'show active' : '' }}" id="custom-tabs-{{ $key }}" role="tabpanel" aria-labelledby="custom-tabs-{{ $key }}-tab">
                <div class="row justify-content-end">
                    <a href="/settings/delete/{{ $key }}" class="btn btn-danger">{{ __('DELETE') }}</a>
                </div>
                @include('partials.settings.type.' . $type, ['key'=> $key, 'value' => $value])
            </div>
            @php
                if($activeSelected) {
                    $activeSelected = false;
                }
            @endphp
        @endforeach
      </div>
    </div>
    <!-- /.card -->
</div>
@else
    <div class="card card-info card-tabs">
        <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
                    <div class="tab-pane fade show active custom-no-record" id="custom-tabs-no-record" role="tabpanel" aria-labelledby="custom-tabs-no-record-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>{{ __('No record found') }}</h1>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
@endif
