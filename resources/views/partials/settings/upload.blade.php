<form action="{{ route('setting.transaction.upload') }}" method="POST" enctype="multipart/form-data">
   @csrf
  <div class="input-group">
    <div class="col-sm-4">
        <input type="file" class="custom-file-input" id="upload_file" name="upload_file" placeholder="Upload a csv file" aria-label="Upload CSV" aria-describedby="Upload CSV">
        <label class="custom-file-label" for="upload_file">{{ __('Choose file') }}</label>
    </div>
    <div class="input-group ml-1 col-sm-2">
        <select class="form-control" name="report_type" id="report_type" aria-label="select">
            <option selected>- Select type -</option>
            <option value="transaction">{{ __('Transaction') }}</option>
            <option value="login">{{ __('Login') }}</option>
          </select>
    </div>
    <div class="col-sm-2">
        <button class="btn btn-primary" id="upload_btn" type="button">{{ __('Start Upload') }}</button>
    </div>
  </div>
</form>
