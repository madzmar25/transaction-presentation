<div class="modal fade" id="delete-user" style="display:none" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Delete User') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            {{ Form::open(array('url' => '/user', 'method' => 'DELETE')) }}
            <div class="modal-body">
                <div>{{ __('Please enter your current password to confirm delete action') }}</div>
                <div class="form-group">
                    {{ Form::label('current_password', 'Password', ['class' => '']) }}
                    {{ Form::password('current_password', ['class' => 'form-control', 'id' => 'current_password', 'required' => true, 'placeholder' => "Enter password"]) }}
                </div>
                <div class="update-message-container user-message-container alert alert-success">{{ __('Successfully updated!') }}</div>
            </div>
            <div class="modal-footer justify-content-between">
                {{ Form::button('Close', ['id' => 'close', 'class' => 'btn btn-default', 'data-dismiss' => "modal"]) }}
                {{ Form::button('Delete User', ['id' => 'delete_user', 'class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
