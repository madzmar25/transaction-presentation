<div class="modal fade" id="edit-user" style="display:none" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Update User') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            {{ Form::open(array('url' => '/user', 'method' => 'PUT')) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('firstname', 'First Name', ['class' => '']) }}
                    {{ Form::text('firstname', '', ['class' => 'form-control', 'id' => 'firstname', 'placeholder' => "Enter first name"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('lastname', 'Last Name', ['class' => '']) }}
                    {{ Form::text('lastname', '', ['class' => 'form-control', 'id' => 'lastname', 'placeholder' => "Enter last name"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Email address', ['class' => '']) }}
                    {{ Form::email('email', '', ['class' => 'form-control', 'id' => 'email', 'placeholder' => "Enter email address"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('status', 'Status', ['class' => '']) }}
                    {{ Form::select('status', ['1' => 'Active', '0' => 'Inactive'], null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'New Password', ['class' => '']) }}
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => "Enter new password"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password-confirm', 'Confirm new password', ['class' => '']) }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm', 'placeholder' => "Confirm new password"]) }}
                </div>
                <hr/>
                <div class="bg-info confirm-password-container">
                    <div>{{ __('Please enter your current password to confirm edit action') }}</div>
                    <div class="form-group">
                        {{ Form::label('current_password', 'Password', ['class' => '']) }}
                        {{ Form::password('current_password', ['class' => 'form-control', 'id' => 'current_password', 'required' => true, 'placeholder' => "Enter password"]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                {{ Form::button('Close', ['id' => 'close', 'class' => 'btn btn-default', 'data-dismiss' => "modal"]) }}
                {{ Form::button('Update User', ['id' => 'update_user', 'class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
