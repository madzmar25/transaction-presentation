<div class="modal fade" id="add-user" style="display:none" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Add User') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            {{ Form::open(array('route' => 'user.register', 'data-redirect' => route('admin.users'))) }}
            <div class="modal-body">
                <div class="message"></div>
                <div class="form-group">
                    {{ Form::label('firstname', 'First Name', ['class' => '']) }}
                    {{ Form::text('firstname', '', ['class' => 'form-control', 'id' => 'firstname', 'required' => true, 'placeholder' => "Enter first name"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('lastname', 'Last Name', ['class' => '']) }}
                    {{ Form::text('lastname', '', ['class' => 'form-control', 'id' => 'lastname', 'required' => true, 'placeholder' => "Enter last name"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Email address', ['class' => '']) }}
                    {{ Form::email('email', '', ['class' => 'form-control', 'id' => 'email', 'required' => true, 'placeholder' => "Enter email address"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Password', ['class' => '']) }}
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'required' => true, 'placeholder' => "Enter password"]) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password-confirm', 'Confirm Password', ['class' => '']) }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm', 'required' => true, 'placeholder' => "Confirm password"]) }}
                </div>
                <div class="bg-info confirm-password-container">
                    <div>{{ __('Please enter your current password to confirm add action') }}</div>
                    <div class="form-group">
                        {{ Form::label('current_password', 'Password', ['class' => '']) }}
                        {{ Form::password('current_password', ['class' => 'form-control', 'id' => 'current_password', 'required' => true, 'placeholder' => "Enter password"]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                {{ Form::button('Close', ['id' => 'close', 'class' => 'btn btn-default', 'data-dismiss' => "modal"]) }}
                {{ Form::button('Save User', ['id' => 'save_user', 'class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
