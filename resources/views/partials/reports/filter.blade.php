<form class="form-inline" action="{{ route('reports.search') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="field1" class="col-form-label">{{ __('Date and time range:') }}</label>
        <div class="input-group date ml-3" id="dailylogdate" data-target-input="nearest">
            <div class="input-group-prepend" data-target="#dailylogdate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
            <!--<input type="text" class="form-control float-right" id="reservationtime">-->
            <input type="text" class="form-control datetimepicker-input" name="date" data-target="#dailylogdate"/>
        </div>
        <div class="input-group ml-3">
            @foreach ($report_type as $key => $typeValue)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="report_type" id="{{ $key }}" value="{{ $key }}" @if($key == $type) checked="true" @endif>
                    <label class="form-check-label" for="{{ $key }}">
                        {{ $typeValue }}
                    </label>
                </div>
            @endforeach
        </div>
        <button type="button" id="search-daily-btn" class="btn btn-success ml-3">{{ __('Search') }}</button>
    </div>
</form>
