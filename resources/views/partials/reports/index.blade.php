@if($data && isset($data['data']) && $data['data'])

    <div class="card card-info card-tabs">
        <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                @php
                    $has_selected = false;

                    if ($tab_selected) {
                        $has_selected = true;
                    }
                @endphp
                @foreach ($data['data'] as $column => $value)
                <li class="nav-item">
                    <a class="nav-link {{ (!$has_selected) ? 'active' : (($tab_selected == $column) ? 'active' : '') }}"
                        id="custom-tabs-{{ $column }}-tab"
                        data-toggle="pill"
                        href="#custom-tabs-{{ $column }}"
                        role="tab"
                        aria-controls="custom-tabs-{{ $column }}"
                        aria-selected="true">
                            {{ strtoupper($column) }}
                    </a>
                </li>
                @php
                    if(!$has_selected) {
                        $has_selected = true;
                    }
                @endphp
                @endforeach
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
                <?php
                    $has_selected = false;

                    if ($tab_selected) {
                        $has_selected = true;
                    }
                ?>
                <?php foreach ($data['data'] as $column => $value): ?>
                    <div class="tab-pane fade {{ !$has_selected ? 'show active' : (($tab_selected == $column) ? 'show active' : '') }}" id="custom-tabs-{{ $column }}" role="tabpanel" aria-labelledby="custom-tabs-{{ $column }}-tab">
                        <table class="table table-responsive table-striped">
                            <thead>
                                <th></th>
                                @foreach ($time_column as $c)
                                <th>{{ strtoupper($c) }}</th>
                                @endforeach
                            </thead>
                            <tbody>
                                @php $rows = ['data' => 'Current', 'settings' => 'Average', 'percentage' => 'Percentage']; @endphp
                                @foreach ($rows as $key => $row)
                                    <tr>
                                        <td><?= $row ?></td>
                                        @foreach ($time_column as $c)
                                            @if($key == "percentage")
                                            <td><span class="{{ $column }} {{ $data['class_status'][$column][$c] }}">{{ $data[$key][$column][$c] }} <small>%</small></span></td>
                                            @else
                                            <td>{{ ((isset($data[$key]) && isset($data[$key][$column]) && isset($data[$key][$column][$c])) ? $data[$key][$column][$c] : 0) }}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        @include('partials.graphs.daily-chart', ['key'=> $column, 'data' => $data, 'type' => $type])
                    </div>
                    @php
                        if(!$has_selected) {
                            $has_selected = true;
                        }
                    @endphp
                <?php endforeach; ?>
            </div>
        </div>
    <!-- /.card-body -->
    </div>
@else
<div class="card card-info">
    <div class="card-body">
        <div class="col-md-12">
            <h1 class="no-graph-error">{{ __('No record found') }}</h1>
        </div>
    </div>
</div>
@endif
