@extends('layouts.app')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="card card-info card-tabs">
            <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-search-tab" data-toggle="pill" href="#search-tab" role="tab" aria-controls="custom-search-tab" aria-selected="true">{{ __('Search') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-upload-tab" data-toggle="pill" href="#upload-tab" role="tab" aria-controls="custom-upload-tab" aria-selected="true">{{ __('Upload') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-manual-input-tab" data-toggle="pill" href="#manua-input-tab" role="tab" aria-controls="custom-manual-input-tab" aria-selected="false">{{ __('Download Template') }}</a>
                    </li>
                </ul>
            </div>

            <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                    <div class="tab-pane fade show active" id="search-tab" role="tabpanel" aria-labelledby="search-tab">
                        @include('partials.settings.search')
                    </div>
                    <div class="tab-pane fade" id="upload-tab" role="tabpanel" aria-labelledby="upload-tab">
                    @include('partials.settings.upload')
                    </div>
                    <div class="tab-pane fade" id="manua-input-tab" role="tabpanel" aria-labelledby="manua-input-tab">
                        @include('partials.settings.download')
                    </div>
                </div>
            </div>
        </div>

        <div id="tabs">
            @include('partials.settings.graphs', ['tabs' => $tabs])
        </div>
    </div>
</div>
@endsection
