@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="user-message-container alert"></div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __('Admin User List') }}</h3>
                <div class="float-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-user">{{ __('Add user') }} <i class="fas fa-plus"></i></button>
                </div>
                @include('partials.modals.add_user')
                @include('partials.modals.edit_user')
                @include('partials.modals.delete_user')
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-striped table-hover yajra-datatable display" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>index</th>
                            <th>PID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
