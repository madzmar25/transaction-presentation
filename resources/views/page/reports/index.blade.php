@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="report-message-container alert"></div>
        <div class="card">
            <div class="card-body">
                @include('partials.reports.filter')
            </div>
        </div>
        <div id="chart-container">
            @include('partials.reports.index')
        </div>
    </div>
</div>
@endsection
