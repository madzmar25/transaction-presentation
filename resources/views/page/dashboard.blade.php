@extends('layouts.app')

@section('content')
<div class="content dashboard-container">
    <div class="container-fluid">
        <h3>{{ __('Displaying records for:') }} <span class="timer"></span></h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-info">
                    <div class="card-header">{{ __('Login') }}</div>
                    <div class="card-body login-row">
                        @include('page.dashboard.login')
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="card card-info">
                    <div class="card-header">{{ __('Transaction') }}</div>
                    <div class="card-body transaction-row">
                        @include('page.dashboard.transaction')
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-info">
                    <div class="card-header">{{ __('User Last Login') }}</div>
                    <div class="card-body">
                        <div id="login_user">
                            @include('page.dashboard.user_table', ['users' => $users])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
