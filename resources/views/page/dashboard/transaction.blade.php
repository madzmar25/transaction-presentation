<div class="row">
    <div class="col-md-4">
        <a href="#" id="transaction-success-info" class="info-box bg-success bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-check"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Success') }}</span>
                    <span class="info-box-number success"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-success"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-success"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-warning-info" class="info-box bg-warning bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-times"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Failed') }}</span>
                    <span class="info-box-number failed"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-failed"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-failed"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-cancelled-info" class="info-box bg-warning bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-trash"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Cancelled') }}</span>
                    <span class="info-box-number cancelled"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-cancelled"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-cancelled"></strong></span>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <a href="#" id="transaction-approved-info" class="info-box bg-success bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-check-double"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Approved') }}</span>
                    <span class="info-box-number approved"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-approved"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-approved"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-processing-info" class="info-box bg-primary bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-microchip"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Processing') }}</span>
                    <span class="info-box-number processing"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-processing"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-processing"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-scheduled-info" class="info-box bg-primary bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-calendar-alt"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Scheduled') }}</span>
                    <span class="info-box-number scheduled"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-scheduled"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-scheduled"></strong></span>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <a href="#" id="transaction-for_approval-info" class="info-box bg-primary bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-thumbs-up"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('For approval') }}</span>
                    <span class="info-box-number for_approval"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-for_approval"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-for_approval"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-for_processing-info" class="info-box bg-primary bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-spinner"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('For processing') }}</span>
                    <span class="info-box-number for_processing"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-for_processing"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-for_processing"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-for_otp_verification-info" class="info-box bg-warning bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-key"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('For OTP Verification') }}</span>
                    <span class="info-box-number for_otp_verification"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-for_otp_verification"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-for_otp_verification"></strong></span>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <a href="#" id="transaction-released-info" class="info-box bg-success bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-envelope-open-text"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Released') }}</span>
                    <span class="info-box-number released"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-released"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-released"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-expired-info" class="info-box bg-warning bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-window-close"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Expired') }}</span>
                    <span class="info-box-number expired"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-expired"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-expired"></strong></span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#" id="transaction-rejected-info" class="info-box bg-warning bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-times-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Rejected') }}</span>
                    <span class="info-box-number rejected"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-rejected"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-rejected"></strong></span>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <a href="#" id="transaction-conditional-info" class="info-box bg-primary bg-card">
            <div class="info-box-row">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-check"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">{{ __('Conditional') }}</span>
                    <span class="info-box-number conditional"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box-row detail bg-light">
                <span class="info-box-text">{{ __('Current') }}: <strong id="current-transaction-conditional"></strong></span>
                <span class="info-box-text">{{ __('Average') }}: <strong id="average-transaction-conditional"></strong></span>
            </div>
        </a>
    </div>
</div>

{{--
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "success", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "failed", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "approved", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "for_approval", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "processing", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "for_processing", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "scheduled", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "released", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "cancelled", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "expired", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "for_otp_verification", 'data' => $data, 'type' => "transaction"])
    </div>
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "rejected", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @include('partials.graphs.daily-chart', ['key'=> "conditional", 'data' => $data, 'type' => "transaction"])
    </div>
</div>
--}}
