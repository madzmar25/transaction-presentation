<table class="table table-hover table-striped">
    <thead>
        <th>{{ __('Name') }}</th>
        <th>{{ __('Last Login') }}</th>
        <th>{{ __('Action') }}</th>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td><?= $user->firstname ?></td>
            <td>{{ date('Y-m-d H:i:s', strtotime($user->created_at)) }}</td>
            <td><a href="/users/{{ $user->pid }}" class="btn btn-success">{{ __('View') }}</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
