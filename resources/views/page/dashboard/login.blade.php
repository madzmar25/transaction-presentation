{{--
<div class="row red">
    <div class="col-sm-12">{{ __('Note: Percentage text is colored based on it\'s value relative to the average.') }}</div>
    <ul class="row">
        <li>
            <span class="lower bg-default"> {{ __('RED') }} </span> {{ __('means the current value is lower than the average') }}
        </li>
        <li>
            <span class="higher bg-default"> {{ __('BLACK') }} </span> {{ __('means the current value is higher than the average and') }}
        </li>
        <li>
            <span class="none bg-default"> {{ __('WHITE') }} </span> {{ __('means the current value is the same with the average') }}
        </li>
    </ul>
</div>
--}}
<div class="row">
    <div class="col-md-4">
        <div class="small-box bg-success">

            <div class="inner">
                <h3>{{ __('Success') }}:
                    <span class="success"></span>
                </h3>
                <p>
                    <span>{{ __('Current') }}: <strong id="current-login-success"></strong></span>
                    <span>|</span>
                    <span>{{ __('Average') }}: <strong id="average-login-success"></strong></span>
                </p>
            </div>
            <div class="icon">
                <i class="fas fa-check"></i>
            </div>
            <a href="#" id="login-success-info" class="small-box-footer" data-target="#loginsuccessChart">{{ __('More info') }}
                <i class="fas fa-arrow-circle-down"></i>
            </a>
        </div>
        {{--@include('partials.graphs.daily-chart', ['key'=> "success", 'data' => $dataLogin, 'type' => "login"])--}}
    </div>
    <div class="col-md-4">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ __('Pending') }}:
                    <span class="pending"></span>
                </h3>
                <p>
                    <span>{{ __('Current') }}: <strong id="current-login-pending"></strong></span>
                    <span>|</span>
                    <span>{{ __('Average') }}: <strong id="average-login-pending"></strong></span>
                </p>
            </div>
            <div class="icon">
                <i class="fas fa-clock"></i>
            </div>
            <a href="#" id="login-pending-info" class="small-box-footer" data-target="#loginpendingChart">{{ __('More info') }}
                <i class="fas fa-arrow-circle-down"></i>
            </a>
        </div>
        {{--@include('partials.graphs.daily-chart', ['key'=> "pending", 'data' => $dataLogin, 'type' => "login"])--}}
    </div>
    <div class="col-md-4">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{ __('Failed') }}:
                    <span class="failed"></span>
                </h3>
                <p>
                    <span>{{ __('Current') }}: <strong id="current-login-failed"></strong></span>
                    <span>|</span>
                    <span>{{ __('Average') }}: <strong id="average-login-failed"></strong></span>
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-times"></i>
            </div>
            <a href="#" id="login-failed-info" class="small-box-footer" data-target="#loginfailedChart">{{ __('More info') }}
                <i class="fas fa-arrow-circle-down"></i>
            </a>
        </div>
        {{--@include('partials.graphs.daily-chart', ['key'=> "failed", 'data' => $dataLogin, 'type' => "login"])--}}
    </div>
</div>
